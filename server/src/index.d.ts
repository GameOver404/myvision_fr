interface String {
    toPascalCase(): string;

    toCamelCase(): string;
}
