import fs from 'fs';
import {
    calcHist,
    CV_8UC1,
    CV_8UC3,
    destroyAllWindows,
    imread,
    imshow,
    imshowWait,
    LINE_8,
    Mat,
    plot1DHist,
    Point2,
    Point3,
    Vec2,
    Vec3,
    Vec4
} from 'opencv4nodejs';
import { BLUE_COLOR, decodeBase64, GREEN_COLOR, RED_COLOR } from './utilsOpenCV';

const rows = 100; // height
const cols = 100; // width
const lennaPath = '../webapp/src/assets/images/lenna.png';

// empty Mat
const emptyMat = new Mat(rows, cols, CV_8UC3);
console.log('Empty Mat:', emptyMat);

// fill the Mat with default value
const whiteMat = new Mat(rows, cols, CV_8UC1, 255);
const blueMat = new Mat(rows, cols, CV_8UC3, [255, 0, 0]);
console.log('White Mat:', whiteMat);
console.log('Blue Mat:', blueMat);

// from array (3x3 Matrix, 3 channels)
const matData = [[[255, 0, 0], [255, 0, 0], [255, 0, 0]], [[0, 0, 0], [0, 0, 0], [0, 0, 0]], [[255, 0, 0], [255, 0, 0], [255, 0, 0]]];
const matFromArray = new Mat(matData, CV_8UC3);
console.log(`Mat from Array:\nArray Data:\t${matData}\n`, matFromArray);

// Point
const pt2 = new Point2(100, 100);
console.log('Point 2D:', pt2);
const pt3 = new Point3(100, 100, 0.5);
console.log('Point 3D:', pt3);

// Vector
const vec2 = new Vec2(100, 100);
console.log('Vector 2D:', vec2);
const vec3 = new Vec3(100, 100, 0.5);
console.log('Vector 3D:', vec3);
const vec4 = new Vec4(100, 100, 0.5, 0.5);
console.log('Vector 4D:', vec4);

// Plot histogram
const img = imread(lennaPath);

// single axis for 1D hist
const getHistAxis = (channel: number) => [
    {
        bins: 256,
        channel,
        ranges: [0, 256]
    }
];

// calc histogram for blue, green, red channel
const bHist = calcHist(img, getHistAxis(0));
const gHist = calcHist(img, getHistAxis(1));
const rHist = calcHist(img, getHistAxis(2));

// plot channel histograms
const plot = new Mat(300, 600, CV_8UC3, [255, 255, 255]);
plot1DHist(bHist, plot, BLUE_COLOR, LINE_8, 2);
plot1DHist(gHist, plot, GREEN_COLOR, LINE_8, 2);
plot1DHist(rHist, plot, RED_COLOR, LINE_8, 2);

imshow('RGB Image', img);
console.log('Show RGB Image and Histogram, WAITING FOR KEY STROKE TO CONTINUE!');
imshowWait('RGB Histogram', plot);

destroyAllWindows();

const grayImg = img.bgrToGray();
const grayHist = calcHist(grayImg, getHistAxis(0));
const grayHistPlot = new Mat(300, 600, CV_8UC3, [255, 255, 255]);
plot1DHist(grayHist, grayHistPlot, new Vec3(0, 0, 0));

imshow('greyscale Image', grayImg);
console.log('Show greyscale Image and Histogram, WAITING FOR KEY STROKE TO CONTINUE!');
imshowWait('greyscale Histogram', grayHistPlot);

destroyAllWindows();

const base64Lenna = Buffer.from(fs.readFileSync(lennaPath)).toString('base64');
const decodedImage = decodeBase64(base64Lenna);

console.log('Show decoded base64 image of Lenna, WAITING FOR KEY STROKE TO CONTINUE!');
imshowWait('Decode Base64 Lenna', decodedImage);

destroyAllWindows();
