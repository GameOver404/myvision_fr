import { CascadeClassifier, drawKeyPoints, imdecode, imencode, imshow, Mat, Rect, Vec3, VideoCapture, waitKey } from 'opencv4nodejs';
import { FeatureDetector } from 'opencv4nodejs/lib/typings/FeatureDetector';

export const BLUE_COLOR = new Vec3(255, 0, 0);
export const GREEN_COLOR = new Vec3(0, 255, 0);
export const RED_COLOR = new Vec3(0, 0, 255);

export const grabFrames = (videoFile: any, delay: number, onFrame: (frame: Mat) => void): void => {
    const cap = new VideoCapture(videoFile);
    let done = false;
    const intvl = setInterval(() => {
        let frame = cap.read();
        // loop back to start on end of stream reached
        if (frame.empty) {
            cap.reset();
            frame = cap.read();
        }
        onFrame(frame);

        const key = waitKey(delay);
        done = key !== -1 && key !== 255;
        if (done) {
            clearInterval(intvl);
            console.log('Key pressed, exiting.');
        }
    }, 0);
};

export const runVideoFaceDetection = (win: string, src: string | number, getFaces: (img: Mat) => Rect[]): void => {
    return grabFrames(src, 1, frame => {
        // const timerName = 'Detection Time';
        // console.time(timerName);
        const frameResized = frame.resizeToMax(800);

        // detect faces
        const faceRects = getFaces(frameResized);
        if (faceRects.length) {
            // draw detection
            faceRects.forEach(faceRect => frameResized.drawRectangle(faceRect, GREEN_COLOR));
        }

        imshow(win, frameResized);
        // console.timeEnd(timerName);
    });
};

export const detectFaces = (classifierEnum: string, image: Mat): Rect[] => {
    const classifier = new CascadeClassifier(classifierEnum);
    const detection = classifier.detectMultiScale(image.bgrToGray());
    return detection.objects;
};

export const decodeBase64 = (base64: string): Mat => {
    const pngPrefix = 'data:image/png;base64,';
    const jpgPrefix = 'data:image/jpeg;base64,';
    const base64Data = base64.replace(pngPrefix, '').replace(jpgPrefix, '');
    const buffer = Buffer.from(base64Data, 'base64');
    return imdecode(buffer);
};

export const encodeJpgBase64 = (img: Mat): string => imencode('.jpg', img).toString('base64');

export const detectKeyPoints = (detector: FeatureDetector, image: Mat) => {
    const keyPoints = detector.detect(image);
    return {
        keyPointImage: drawKeyPoints(image, keyPoints),
        keyPoints
    };
};
