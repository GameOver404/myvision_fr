import { CascadeClassifier, destroyAllWindows, HAAR_FRONTALFACE_ALT2, imread, imshowWait, Mat, xmodules } from 'opencv4nodejs';
import { GREEN_COLOR, runVideoFaceDetection } from './utilsOpenCV';

if (!xmodules.face) {
    throw new Error('exiting: opencv4nodejs compiled without face module!');
}

const imagePath = '../webapp/src/assets/images/lenna.png';
const image = imread(imagePath);

const classifier = new CascadeClassifier(HAAR_FRONTALFACE_ALT2);

const getFaceRect = (grayImage: Mat) => {
    const faceRects = classifier.detectMultiScale(grayImage).objects;
    if (!faceRects.length) {
        throw new Error('Failed to detect faces!');
    }
    return faceRects[0];
};

const grayImg = image.bgrToGray();

const faceRect = getFaceRect(grayImg);

console.log('Face Rect:', faceRect);

// Draw face rectangle
image.drawRectangle(faceRect, GREEN_COLOR);

imshowWait('Face Detection (Lenna)', image);

destroyAllWindows();

// Face Detection
runVideoFaceDetection(
    'Face Detection Cam: 0',
    0,
    (img: Mat) => classifier.detectMultiScale(img.bgrToGray() /*, 1.2, 10, 0, new cv.Size(100, 100)*/).objects
);

try {
    runVideoFaceDetection(
        'Face Detection Cam: 1',
        1,
        (img: Mat) => classifier.detectMultiScale(img.bgrToGray() /*, 1.2, 10, 0, new cv.Size(100, 100)*/).objects
    );
} catch (error) {
    console.error('Webcam 2 error:', error);
}

destroyAllWindows();
