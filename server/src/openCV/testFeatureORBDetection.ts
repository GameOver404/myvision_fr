import { destroyAllWindows, imshow, ORBDetector } from 'opencv4nodejs';
import { detectKeyPoints, grabFrames } from './utilsOpenCV';

grabFrames(0, 1, frame => {
    const orbDetector = new ORBDetector();

    imshow('Feature Detection (ORB)', detectKeyPoints(orbDetector, frame).keyPointImage);
});

destroyAllWindows();
