import { destroyAllWindows, imshow, SURFDetector } from 'opencv4nodejs';
import { detectKeyPoints, grabFrames } from './utilsOpenCV';

grabFrames(0, 1, frame => {
    const surfDetector = new SURFDetector();

    imshow('Feature Detection (SURF)', detectKeyPoints(surfDetector, frame).keyPointImage);
});

destroyAllWindows();
