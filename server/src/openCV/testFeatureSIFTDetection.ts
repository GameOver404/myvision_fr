import { destroyAllWindows, imshow, SIFTDetector } from 'opencv4nodejs';
import { detectKeyPoints, grabFrames } from './utilsOpenCV';

grabFrames(0, 1, frame => {
    const siftDetector = new SIFTDetector();

    imshow('Feature Detection (SIFT)', detectKeyPoints(siftDetector, frame).keyPointImage);
});

destroyAllWindows();
