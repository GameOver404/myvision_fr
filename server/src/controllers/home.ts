import { Request, Response } from 'express';

/**
 * GET /
 * Returns hello msg
 */
export const homeIndex = (req: Request, res: Response) => res.json({ msg: 'Hello World!' });
