import { Request, Response } from 'express';
import { check, validationResult } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';
import { PersonModel } from '../models/Person';

export const validatePostAddPerson = [
    check('name')
        .exists() // Check if name data exits
        .withMessage('The name of the person must be given in the name parameter!')
        .isString() // Check if name data is a string
        .withMessage('The name parameter must be a string!')
];

export const postAddPerson = (req: Request, res: Response) => {
    // Get the validation result
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }

    // matchedData returns only the subset of data validated by the middleware
    const data = matchedData(req);

    PersonModel.findOneAndUpdate({ name: data.name }, data, { upsert: true })
        .then(doc =>
            res.json({
                msg: 'Person added'
            })
        )
        .catch(error =>
            res.status(400).json({
                errors: {
                    msg: error
                }
            })
        );
};
