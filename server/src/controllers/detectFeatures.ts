import { Request, Response } from 'express';
import { check, validationResult } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';
import { FeatureDetector, ORBDetector, SIFTDetector, SURFDetector } from 'opencv4nodejs';
import { decodeBase64, detectKeyPoints, encodeJpgBase64 } from '../openCV/utilsOpenCV';

const FEATURE_DETECTORS: { [key: string]: FeatureDetector } = {
    orb: new ORBDetector(),
    sift: new SIFTDetector(),
    surf: new SURFDetector()
};

export const validatePostDetectFeatures = [
    check('image')
        .exists() // Check if image data exits
        .withMessage('The image data must be given in the image parameter!')
        .isBase64() // Check if image data is base64
        .withMessage('The image data must be base64!'),
    check('detector').custom(value => {
        if (!!value) {
            const featureDetectorKeys = Object.keys(FEATURE_DETECTORS);
            if (!featureDetectorKeys.includes(value)) {
                throw new Error(
                    'The detector must either be empty (than the ORB will be used), ' +
                        'or if given it must be one of this values: ' +
                        featureDetectorKeys.join(', ')
                );
            }
        }
        return true;
    })
];

export const postDetectFeatures = (req: Request, res: Response) => {
    // Get the validation result
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }

    // matchedData returns only the subset of data validated by the middleware
    const data = matchedData(req);

    const featureDetector = data.detector ? FEATURE_DETECTORS[data.detector] : FEATURE_DETECTORS.orb;

    const featureDetections = detectKeyPoints(featureDetector, decodeBase64(data.image));

    return res.json({
        count: featureDetections.keyPoints.length,
        image: encodeJpgBase64(featureDetections.keyPointImage),
        msg: 'Detected Features'
    });
};
