import { Request, Response } from 'express';
import { check, validationResult } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';
import {
    HAAR_FRONTALFACE_ALT,
    HAAR_FRONTALFACE_ALT2,
    HAAR_FRONTALFACE_ALT_TREE,
    HAAR_FRONTALFACE_DEFAULT,
    HAAR_PROFILEFACE,
    LBP_FRONTALFACE,
    LBP_FRONTALFACE_IMPROVED,
    LBP_PROFILEFACE
} from 'opencv4nodejs';
import { decodeBase64, detectFaces } from '../openCV/utilsOpenCV';

const FACE_CLASSIFIERS: { [key: string]: string } = {
    haar_front_alt: HAAR_FRONTALFACE_ALT,
    haar_front_alt2: HAAR_FRONTALFACE_ALT2,
    haar_front_alt3: HAAR_FRONTALFACE_ALT_TREE,
    haar_front_default: HAAR_FRONTALFACE_DEFAULT,
    haar_profile: HAAR_PROFILEFACE,
    lbp_front: LBP_FRONTALFACE,
    lbp_front2: LBP_FRONTALFACE_IMPROVED,
    lbp_profile: LBP_PROFILEFACE
};

export const validatePostDetectFaces = [
    check('image')
        .exists() // Check if image data exits
        .withMessage('The image data must be given in the image parameter!')
        .isBase64() // Check if image data is base64
        .withMessage('The image data must be base64!'),
    check('classifier').custom(value => {
        if (!!value) {
            const faceClassifierKeys = Object.keys(FACE_CLASSIFIERS);
            if (!faceClassifierKeys.includes(value)) {
                throw new Error(
                    'The classifier must either be empty (then the haar_front_alt2 will be used), ' +
                        'or if given it must be one of this values: ' +
                        faceClassifierKeys.join(', ')
                );
            }
        }
        return true;
    })
];

export const postDetectFaces = (req: Request, res: Response) => {
    // Get the validation result
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.mapped() });
    }

    // matchedData returns only the subset of data validated by the middleware
    const data = matchedData(req);

    const faceClassifier = data.classifier ? FACE_CLASSIFIERS[data.classifier] : FACE_CLASSIFIERS.haar_front_alt2;

    const faces = detectFaces(faceClassifier, decodeBase64(data.image));

    return res.json({
        count: faces.length,
        faces,
        msg: 'Detected faces (rectangles)'
    });
};
