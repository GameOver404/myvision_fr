String.prototype.toPascalCase = function(): string {
    return this.split(' ')
        .map(item => item.charAt(0).toUpperCase() + item.substr(1))
        .join('');
};

String.prototype.toCamelCase = function(): string {
    return this.split(' ')
        .map((item, index) => (index === 0 ? item.charAt(0).toLowerCase() : item.charAt(0).toUpperCase() + item.substr(1)))
        .join('');
};

export const toPascalCase = (str: string): string => str.toPascalCase();

export const toCamelCase = (str: string): string => str.toCamelCase();
