import errorHandler from 'errorhandler';

import app from './App';

/**
 * Error Handler. Provides full stack- remove for production
 */
app.use(errorHandler());

const port = app.get('port');

/**
 * Start Express server.
 */
const server = app.listen(port, () =>
    console.log(`
    Server is running at http://localhost:${port} in ${app.get('env')} mode.
    Press CTRL-C to stop\n`)
);

export default server;
