import { arrayProp } from 'typegoose';
import { Person } from './Person';
import { TimestampName } from './TimestampName';

class Group extends TimestampName {
    @arrayProp({ items: Person })
    public persons?: Person[];
}

export const GroupModel = new Group().getTimestampModelForClass(Group);
