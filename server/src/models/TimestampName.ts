import { ModelType, prop, staticMethod, Typegoose } from 'typegoose';
import { Person } from './Person';

/**
 * This class can be used as a base class if in the schema options the timestamps are enabled
 */
export abstract class TimestampName extends Typegoose {
    @staticMethod
    public static findByName(this: ModelType<Person> & typeof Person, name: string) {
        return this.findOne({ name });
    }

    @prop({ required: true })
    public name: string;

    @prop() public createdAt: Date;
    @prop() public updatedAt: Date;

    public getTimestampModelForClass<T>(t: T) {
        return super.getModelForClass(t, { schemaOptions: { timestamps: true } });
    }
}
