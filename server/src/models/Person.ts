import { prop } from 'typegoose';
import { toPascalCase } from '../utils';
import { TimestampName } from './TimestampName';

export class Person extends TimestampName {
    @prop() public images?: string[];

    // virtual prop
    @prop()
    public get label() {
        return toPascalCase(this.name);
    }
}

export const PersonModel = new Person().getTimestampModelForClass(Person);
