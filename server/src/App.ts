import { json, urlencoded } from 'body-parser';
import compression from 'compression';
import express, { Express } from 'express';
import expressValidator from 'express-validator';
import { connect } from 'mongoose';
import { postAddPerson, validatePostAddPerson } from './controllers/addPerson';
import { postDetectFaces, validatePostDetectFaces } from './controllers/detectFaces';
import { postDetectFeatures, validatePostDetectFeatures } from './controllers/detectFeatures';
import { homeIndex } from './controllers/home';
import { PersonModel } from './models/Person';

class App {
    public express: Express;

    private readonly REQUEST_BODY_SIZE_LIMIT = '16mb';

    private readonly MONGODB_ADDRESS = 'localhost';
    private readonly MONGODB_PORT = 27017;
    private readonly MONGODB_DATABASE_NAME = 'face-recognition-server';

    constructor() {
        this.express = express();
        this.connectToMongoDB();
        this.setupExpressConfig();
        this.defineRoutes();
        this.testMongoDB();
    }

    private connectToMongoDB() {
        const mongoUrl = `mongodb://${this.MONGODB_ADDRESS}:${this.MONGODB_PORT}/${this.MONGODB_DATABASE_NAME}`;
        connect(mongoUrl)
            .then(() => {
                // ready to use. The `mongoose.connect()` promise resolves to undefined'
            })
            .catch(error => console.error('MongoDB connection error. Please make sure MongoDB is running: ', error));
    }

    private async testMongoDB() {
        // PersonModel is a regular Mongoose Model with correct types
        const person = { name: 'Max Mustermann' };
        await PersonModel.findOneAndUpdate(
            person,
            person,
            { upsert: true },
            err => (err ? console.error('Error save person, error: ', err) : console.log('Successfully saved/updated muster person'))
        );

        const persons = await PersonModel.find(person);
        persons.forEach(p => console.log(JSON.stringify({ ...p.toJSON(), label: p.label }, null, 4)));

        await PersonModel.findOneAndRemove(person);
        console.log('Cleared muster person');
    }

    private setupExpressConfig() {
        this.express.set('port', process.env.PORT || 3000);
        this.express.use(compression());
        this.express.use(
            json({
                limit: this.REQUEST_BODY_SIZE_LIMIT
            })
        );
        this.express.use(
            urlencoded({
                extended: true,
                limit: this.REQUEST_BODY_SIZE_LIMIT
            })
        );
        this.express.use(expressValidator());
    }

    private defineRoutes() {
        this.express.get('/', homeIndex);
        // detection routes
        this.express.post('/detectFaces', validatePostDetectFaces, postDetectFaces);
        this.express.post('/detectFeatures', validatePostDetectFeatures, postDetectFeatures);
        // recognition routes
        this.express.post('/addPerson', validatePostAddPerson, postAddPerson);
    }
}

export default new App().express;
