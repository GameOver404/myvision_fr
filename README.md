# Computer Vision (CV) Bot

This project contains:
* backend (Node.js with Nest.js) in the backend folder - for more information see [Backend Readme](./backend/README.md#Description) &
* frontend (with Angular) in the webapp folder - for more information see 
[Frontend Readme](./webapp/README.md#Description).

Face Recognition, Emotion Recognition and Age Detection functionality 

## Prerequisites

This project requires the following dependencies to be installed beforehand:
* node.js
* npm

#### Install node.js and npm

One can install Node.js from [https://nodejs.org/en/download/](https://nodejs.org/en/download/) - for both Windows and macOS with or without the help of a package manager.
A package manager, like the recommended node version manager - or nvm for short - is very helpful - especially when dealing with permissions. To install nvm run:
$ env VERSION=`python tools/getnodeversion.py` make install DESTDIR=`nvm_version_path v$VERSION` PREFIX="" 
on any operating system.

Linux users should be using [APT](http://www.stsci.edu/hst/proposing/apt/installation/linux) - instead of brew. To install node.js and npm just run
```
sudo apt install nodejs npm
```

Node.js can easily be installed via Homebrew [https://brew.sh/index_de.html](https://brew.sh/index_de.html). Just install brew with
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install"
```
and install node.js 8 by executing
```
brew install node@8
```

The node.js installation contains npm, so you don't have to install it seperately. 
Make sure to update npm:
```
npm install -g npm
```
If you run into problems regarding missing file permissions for npm, take a look at [this link](https://docs.npmjs.com/getting-started/fixing-npm-permissions).

## Getting started

To install project dependencies just run:

```
npm install
```

## Security Checks

The project uses the now archieved [nsp](https://github.com/nodesecurity/nsp) to check packages against [known vulnerabilities](https://nodesecurity.io/advisories/). Furthermore, it uses [RetireJS](https://github.com/RetireJS/retire.js) to check the node and JavaScript files.
