import { Controller, Get } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';

import { AppService } from './app.service';
import { APP_ROUTE } from './constants';

@ApiUseTags(APP_ROUTE)
@Controller(APP_ROUTE)
export class AppController {
    constructor(private readonly appService: AppService) {}

    @Get()
    public root(): string {
        return this.appService.root();
    }
}
