import * as NPMPackage from '../package.json';

// local, private variables
const appName = NPMPackage.name as string;

// APP
export const APP_PORT = 3000;
export const APP_ROUTE = 'api/v1/';
export const REQUEST_BODY_SIZE_LIMIT = '16mb';

// SWAGGER
export const APP_TITLE = appName
    .split('-')
    .map(word => word[0].toUpperCase() + word.slice(1))
    .join(' ');
export const APP_DESCRIPTION = NPMPackage.description as string;
export const APP_VERSION = NPMPackage.version as string;
export const SWAGGER_ROUTE = 'swagger';

// DATABASE
export const MONGODB_ADDRESS = 'localhost';
export const MONGODB_PORT = 27017;
export const MONGODB_DATABASE_NAME = appName;

// PERSON
export const PERSON_ROUTE = APP_ROUTE + 'person';

// COLLECTION
export const COLLECTION_ROUTE = APP_ROUTE + 'collection';

// FACE
export const FACE_ROUTE = APP_ROUTE + 'face';

// RECOGNITION
export const RECOGNITION_ROUTE = APP_ROUTE + 'recognition';

// AGE
export const AGE_DETECTION_MODEL_PATH = '/models/model.json';
export const TRAINING_ERROR_METHOD = 'meanSquaredError';
export const TRAINING_OPTIMIZER = 'sgd';

//Liveness
export const LIVENESS_DETECTION_MODEL_PATH = '/model/model.json';

// EMOTION
export const EMOTIONS = ['neutral', 'anger', 'disgust', 'fear', 'happy', 'sadness', 'surprise'];

