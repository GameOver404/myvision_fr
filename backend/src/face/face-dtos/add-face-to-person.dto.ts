import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class AddFaceToPersonDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly personId!: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly name!: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly image!: string;
}
