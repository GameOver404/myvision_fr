import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class DeleteFaceFromPersonDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly faceId!: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly personId!: string;
}
