import { Test, TestingModule } from '@nestjs/testing';

import { FaceController } from '@face/face.controller';
import { FaceService } from '@face/face.service';

describe('Face Controller', () => {
    let module: TestingModule;
    beforeAll(async () => {
        module = await Test.createTestingModule({
            controllers: [FaceController],
            providers: [
                {
                    provide: FaceService,
                    useValue: 'faceService'
                }
            ]
        }).compile();
    });
    it('should be defined', () => {
        const controller: FaceController = module.get<FaceController>(FaceController);
        expect(controller).toBeDefined();
    });
});
