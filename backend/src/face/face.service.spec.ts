import { Test, TestingModule } from '@nestjs/testing';

import { FaceService } from '@face/face.service';

describe('FaceService', () => {
    let service: FaceService;
    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                FaceService,
                {
                    provide: 'FaceDatabaseService',
                    useValue: 'collectionDatabaseSpy'
                },
                {
                    provide: 'RecognitionService',
                    useValue: 'RecognitionServiceSpy'
                }
            ]
        }).compile();
        service = module.get<FaceService>(FaceService);
    });
    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
