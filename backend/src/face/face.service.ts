import { Injectable } from '@nestjs/common';
import { FaceDatabaseService } from '../database/face-database/face-database.service';
import { FaceObject } from '../interfaces/database-face.interface';
import { RecognitionService } from '../recognition/recognition.service';

@Injectable()
export class FaceService {
    constructor(private readonly faceDatabaseService: FaceDatabaseService, private readonly recognitionService: RecognitionService) {}

    public async addFaceToPerson(personId: string, name: string, image: string): Promise<string> {
        const faceResults = await this.recognitionService.addFaceToPerson(personId, image);
        return await this.faceDatabaseService.addFaceToPerson(personId, name, faceResults);
    }

    public async deleteFaceFromPerson(personId: string, id: string): Promise<string> {
        return await this.faceDatabaseService.deleteFaceFromPerson(personId, id);
    }

    public async listFacesFromPerson(personId: string): Promise<FaceObject[]> {
        return await this.faceDatabaseService.listFacesFromPerson(personId);
    }
}
