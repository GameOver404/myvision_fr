import { Module } from '@nestjs/common';
import { FaceDatabaseModule } from '../database/face-database/face-database.module';
import { RecognitionModule } from '../recognition/recognition.module';
import { FaceController } from './face.controller';
import { FaceService } from './face.service';

@Module({
    imports: [FaceDatabaseModule, RecognitionModule],
    providers: [FaceService],
    controllers: [FaceController]
})
export class FaceModule {}
