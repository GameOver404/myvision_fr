import { Body, Controller, Delete, Get, Param, Post, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { FACE_ROUTE } from '../constants';
import { FaceObject } from '../interfaces/database-face.interface';
import { AddFaceToPersonDto } from './face-dtos/add-face-to-person.dto';
import { DeleteFaceFromPersonDto } from './face-dtos/delete-face-from-person.dto';
import { FaceService } from './face.service';

@ApiUseTags(FACE_ROUTE)
@Controller(FACE_ROUTE)
export class FaceController {
    constructor(private readonly faceService: FaceService) {}

    @ApiOperation({ title: 'Add Face to Person' })
    @Post()
    public addFaceToPerson(
        @Body(new ValidationPipe())
        addFaceToPersonDto: AddFaceToPersonDto
    ): Promise<string> {
        return this.faceService.addFaceToPerson(addFaceToPersonDto.personId, addFaceToPersonDto.name, addFaceToPersonDto.image);
    }

    @ApiOperation({ title: 'Delete Face from Person' })
    @Delete()
    public deleteFaceFromPerson(
        @Body(new ValidationPipe())
        deleteFaceFromPersonDto: DeleteFaceFromPersonDto
    ): Promise<string> {
        return this.faceService.deleteFaceFromPerson(deleteFaceFromPersonDto.personId, deleteFaceFromPersonDto.faceId);
    }

    @ApiOperation({ title: 'List Faces from Person' })
    @Get(':id')
    public listFacesFromPerson(@Param('id') id: string): Promise<FaceObject[]> {
        return this.faceService.listFacesFromPerson(id);
    }
}
