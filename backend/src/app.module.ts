import { CorsMiddleware } from '@nest-middlewares/cors';
import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CollectionModule } from './collection/collection.module';
import { DatabaseModule } from './database/database.module';
import { FaceModule } from './face/face.module';
import { PersonModule } from './person/person.module';
import { RecognitionModule } from './recognition/recognition.module';

@Module({
    imports: [DatabaseModule, PersonModule, CollectionModule, FaceModule, RecognitionModule],
    controllers: [AppController],
    providers: [AppService]
})
export class AppModule implements NestModule {
    public configure(consumer: MiddlewareConsumer): MiddlewareConsumer | void {
        CorsMiddleware.configure({
            origin: 'http://localhost:4200', // TODO: set to correct values (deployment address, port, ...)
            methods: ['GET', 'POST', 'OPTIONS', 'PUT', 'PATCH', 'DELETE'],
            allowedHeaders: ['X-Requested-With', 'content-type'],
            credentials: true
        });
        consumer.apply(CorsMiddleware).forRoutes('*');
    }
}
