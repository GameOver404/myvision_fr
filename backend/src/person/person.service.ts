import { Injectable } from '@nestjs/common';
import { PersonDatabaseService } from '../database/person-database/person-database.service';
import { Person } from '../database/person-database/person.model';

@Injectable()
export class PersonService {
    constructor(private readonly personCollectionService: PersonDatabaseService) {}

    public async addPersonToCollection(collectionId: string, name: string): Promise<string> {
        return await this.personCollectionService.addPersonToCollection(collectionId, name);
    }

    public async deletePersonFromCollection(id: string): Promise<string> {
        return await this.personCollectionService.deletePersonFromCollection(id);
    }

    public async listPersonsFromCollection(collectionId: string): Promise<Person[]> {
        return await this.personCollectionService.listPersonsFromCollection(collectionId);
    }
    public async getPerson(personId: string): Promise<Person> {
        return await this.personCollectionService.getPerson(personId);
    }
}
