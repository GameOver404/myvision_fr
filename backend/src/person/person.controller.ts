import { Body, Controller, Delete, Get, Param, Post, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { PERSON_ROUTE } from '../constants';
import { Person } from '../database/person-database/person.model';
import { AddPersonToCollectionDto } from './person-dtos/add-person-to-collection.dto';
import { PersonService } from './person.service';

@ApiUseTags(PERSON_ROUTE)
@Controller(PERSON_ROUTE)
export class PersonController {
    constructor(private readonly personService: PersonService) {}

    @ApiOperation({ title: 'Add Person to collection' })
    @Post()
    public addPersonToCollection(
        @Body(new ValidationPipe())
        addPersonToCollectionDto: AddPersonToCollectionDto
    ): Promise<string> {
        return this.personService.addPersonToCollection(addPersonToCollectionDto.collectionId, addPersonToCollectionDto.name);
    }

    @ApiOperation({ title: 'Delete Person from collection' })
    @Delete(':id')
    public deletePersonFromCollection(@Param('id') id: string): Promise<string> {
        return this.personService.deletePersonFromCollection(id);
    }

    @ApiOperation({ title: 'List Persons from collection' })
    @Get(':id')
    public listPersonsFromCollection(@Param('id') id: string): Promise<Person[]> {
        return this.personService.listPersonsFromCollection(id);
    }

    @ApiOperation({ title: 'getPersonInformations' })
    @Get('getPerson/:id')
    public getPerson(@Param('id') id: string): Promise<Person> {
        return this.personService.getPerson(id);
    }
}
