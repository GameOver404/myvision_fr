import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class DeletePersonFromCollectionDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly personId!: string;
}
