import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class AddPersonToCollectionDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly name!: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly collectionId!: string;
}
