import { Module } from '@nestjs/common';
import { PersonDatabaseModule } from '../database/person-database/person-database.module';
import { PersonController } from './person.controller';
import { PersonService } from './person.service';

@Module({
    imports: [PersonDatabaseModule],
    providers: [PersonService],
    controllers: [PersonController]
})
export class PersonModule {}
