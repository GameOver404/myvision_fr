import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { MONGODB_ADDRESS, MONGODB_DATABASE_NAME, MONGODB_PORT } from '../constants';
import { CollectionDatabaseModule } from './collection-database/collection-database.module';
import { FaceDatabaseModule } from './face-database/face-database.module';
import { PersonDatabaseModule } from './person-database/person-database.module';
import { TrainDatabaseModule } from './train-database/train-database.module';

@Module({
    imports: [
        TypegooseModule.forRoot(`mongodb://${MONGODB_ADDRESS}:${MONGODB_PORT}/${MONGODB_DATABASE_NAME}`),
        CollectionDatabaseModule,
        FaceDatabaseModule,
        PersonDatabaseModule,
        TrainDatabaseModule
    ]
})
export class DatabaseModule {}
