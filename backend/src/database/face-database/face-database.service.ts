import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';

import { DatabaseFaceService, FaceObject } from '../../interfaces/database-face.interface';
import { Person } from '../person-database/person.model';
import { Face } from './face.model';

@Injectable()
export class FaceDatabaseService implements DatabaseFaceService {
    constructor(
        @InjectModel(Face) private readonly faceModel: ModelType<Face>,
        @InjectModel(Person) private readonly personModel: ModelType<Person>
    ) {}

    public async addFaceToPerson(personId: string, name: string, faceDescriptors: number[]): Promise<string> {
        const createdFace = new this.faceModel({
            name,
            faceDescriptors
        });
        return await this.personModel.update({ _id: personId }, { $push: { faces: createdFace } });
    }

    public async deleteFaceFromPerson(personId: string, id: string): Promise<string> {
        await this.personModel.update({ _id: personId }, { $pull: { faces: { _id: id } } });
        return await this.faceModel.remove({ _id: id });
    }

    public async listFacesFromPerson(personId: string): Promise<FaceObject[]> {
        const faces = await this.personModel
            .findOne(
                { _id: personId },
                {
                    _id: 0,
                    faces: 1
                }
            )
            .exec();

        return faces && faces.faces ? faces.faces.map(face => ({ id: face._id, name: face.name })) : [];
    }
}
