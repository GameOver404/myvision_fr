import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { Person } from '../person-database/person.model';
import { FaceDatabaseService } from './face-database.service';
import { Face } from './face.model';

@Module({
    imports: [TypegooseModule.forFeature(Person), TypegooseModule.forFeature(Face)],
    providers: [FaceDatabaseService],
    exports: [FaceDatabaseService]
})
export class FaceDatabaseModule {}
