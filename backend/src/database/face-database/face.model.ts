import { IsString } from 'class-validator';
import { prop, Typegoose } from 'typegoose';

export class Face extends Typegoose {
    public _id!: string;

    @IsString()
    @prop({ required: true })
    public name!: string;

    @prop() public faceDescriptors!: number[];
}
