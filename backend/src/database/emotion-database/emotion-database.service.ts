import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';
import { EmotionDatabaseData } from '../../../interfaces/recognition.interface';
import { EmotionImage } from './emotion-image.model';

@Injectable()
export class EmotionDatabaseService {
    constructor(@InjectModel(EmotionImage) private readonly emotionModel: ModelType<EmotionImage>) {}

    public async addEmotionImage(emotion: string, image: string, landmarks: number[]): Promise<string> {
        const createEmotionImage = new this.emotionModel({
            emotion,
            image,
            landmarks
        });
        return await createEmotionImage.save().then(() => 'addedSuccessfully');
    }

    public async getTrainingData(): Promise<EmotionDatabaseData[]> {
        return await this.emotionModel.find().then(data =>
            data.map(emotionData => ({
                emotion: emotionData.emotion,
                image: emotionData.image,
                landmarks: emotionData.landmarks
            }))
        );
    }
}
