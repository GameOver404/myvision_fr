import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { Face } from '../face-database/face.model';
import { Person } from '../person-database/person.model';
import { CollectionDatabaseService } from './collection-database.service';
import { Collection } from './collection.model';

@Module({
    imports: [TypegooseModule.forFeature(Person), TypegooseModule.forFeature(Collection), TypegooseModule.forFeature(Face)],
    providers: [CollectionDatabaseService],
    exports: [CollectionDatabaseService]
})
export class CollectionDatabaseModule {}
