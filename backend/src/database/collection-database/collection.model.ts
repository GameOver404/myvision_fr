import { IsString } from 'class-validator';
import { prop, Typegoose } from 'typegoose';

export class Collection extends Typegoose {
    public _id!: string;

    @IsString()
    @prop({ required: true, unique: true })
    public name!: string;
}
