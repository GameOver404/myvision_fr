import { ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { FaceDescriptor } from 'face-recognition';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';
import { DatabaseCollectionService } from '../../interfaces/database-collection.interface';
import { Face } from '../face-database/face.model';
import { Person } from '../person-database/person.model';
import { Collection } from './collection.model';

@Injectable()
export class CollectionDatabaseService implements DatabaseCollectionService {
    constructor(
        @InjectModel(Collection) private readonly collectionModel: ModelType<Collection>,
        @InjectModel(Person) private readonly personModel: ModelType<Person>,
        @InjectModel(Face) private readonly faceModel: ModelType<Face>
    ) {}

    public async listCollections(): Promise<Collection[]> {
        return await this.collectionModel.find().exec();
    }

    public async createCollection(name: string): Promise<string> {
        const createdCollection = new this.collectionModel({ name });
        return createdCollection
            .save()
            .then(res => res._id)
            .catch(() => new ConflictException(`A collection with the name #${name} already exists`));
    }

    public async deleteCollection(id: string): Promise<string> {
        await this.personModel.remove({ collectionId: id });
        const deleteSuccessful = await this.collectionModel.remove({ _id: id });
        return deleteSuccessful.n === 1 ? deleteSuccessful : new NotFoundException(`A Collection with id: ${id} dont exists`);
    }

    public async trainCollection(collectionId: string): Promise<FaceDescriptor[]> {
        const persons = await this.personModel.find({ collectionId }).exec();
        let trainedData: FaceDescriptor[] = [];
        if (persons) {
            trainedData = persons.filter((person: Person) => person.faces.length > 0).map((person: Person) => ({
                className: person._id,
                faceDescriptors: person.faces.map(face => face.faceDescriptors)
            }));
        } else {
            throw new NotFoundException(`No Training Data found`);
        }
        return trainedData;
    }
}
