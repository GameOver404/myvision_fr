import { Module } from '@nestjs/common';
import { EmotionDatabaseModule } from './emotion-database/emotion-database.module';


@Module({
    imports: [EmotionDatabaseModule],
    providers: [],
    exports: []
})
export class TrainDatabaseModule {}
