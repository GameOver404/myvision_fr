import { Module } from '@nestjs/common';
import { AgeDatabaseModule } from './age-database/age-database.module';

@Module({
    imports: [AgeDatabaseModule],
    providers: [],
    exports: []
})
export class TrainDatabaseModule {}
