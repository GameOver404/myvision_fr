import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { EmotionDatabaseService } from './emotion-database.service';
import { EmotionImage } from './emotion-image.model';

@Module({
    imports: [TypegooseModule.forFeature(EmotionImage)],
    providers: [EmotionDatabaseService],
    exports: [EmotionDatabaseService]
})
export class EmotionDatabaseModule {}
