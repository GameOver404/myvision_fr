import { IsNumber, IsString } from 'class-validator';
import { arrayProp, prop,  Ref, Typegoose } from 'typegoose';



export class EmotionImage extends Typegoose {

    @IsString()
    @prop({ required: true})
    public emotion!: string;

    @IsString()
    @prop({ required: true, index:false })
    public image!: string;

    @IsNumber()
    @arrayProp({items:Number})
    public landmarks!: number[];
}
