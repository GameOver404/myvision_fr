import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { AgeDatabaseService } from './age-database.service';
import { AgeImage } from './age-image.model';

@Module({
    imports: [TypegooseModule.forFeature(AgeImage)],
    providers: [AgeDatabaseService],
    exports: [AgeDatabaseService]
})
export class AgeDatabaseModule {}
