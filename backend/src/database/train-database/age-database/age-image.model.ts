import { IsNumber, IsString } from 'class-validator';
import { prop, Typegoose } from 'typegoose';

export class AgeImage extends Typegoose {
    @IsNumber()
    @prop({ required: true })
    public age!: number;

    @IsString()
    @prop({ required: true, index: false })
    public image!: string;
}
