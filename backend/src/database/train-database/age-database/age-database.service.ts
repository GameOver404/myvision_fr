import { Injectable } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';
import { AgeDatabaseData } from '../../../interfaces/recognition.interface';
import { AgeImage } from './age-image.model';

@Injectable()
export class AgeDatabaseService {
    constructor(@InjectModel(AgeImage) private readonly ageModel: ModelType<AgeImage>) {}

    public async addAgeImage(age: number, image: string): Promise<string> {
        const createAgeImage = new this.ageModel({
            age,
            image
        });
        return await createAgeImage.save().then(() => 'addedSuccessfully');
    }

    public async loadAgeData(): Promise<AgeDatabaseData[]> {
        return await this.ageModel.find().then(data =>
            data.map(ageImage => ({
                age: ageImage.age,
                image: ageImage.image
            }))
        );
    }
}
