import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from 'nestjs-typegoose';
import { ModelType } from 'typegoose';
import { DatabasePersonService } from '../../interfaces/database-person.interface';
import { Face } from '../face-database/face.model';
import { Person } from './person.model';

@Injectable()
export class PersonDatabaseService implements DatabasePersonService {
    constructor(
        @InjectModel(Person) private readonly personModel: ModelType<Person>,
        @InjectModel(Face) private readonly faceModel: ModelType<Face>
    ) {}

    public async addPersonToCollection(collectionId: string, name: string): Promise<string> {
        const createdPerson = new this.personModel({
            collectionId,
            name
        });
        return (await createdPerson.save())._id;
    }

    public async deletePersonFromCollection(id: string): Promise<string> {
        return await this.personModel.remove({ _id: id });
    }

    public async listPersonsFromCollection(collectionId: string): Promise<Person[]> {
        return await this.personModel.find({ collectionId }).exec();
    }

    public async getPerson(personId: string): Promise<Person> {
        const person = await this.personModel.findOne({ _id: personId }).exec();
        if (person) {
            return person;
        } else {
            throw new NotFoundException('No Person with this Id exists');
        }
    }
}
