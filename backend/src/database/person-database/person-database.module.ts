import { Module } from '@nestjs/common';
import { TypegooseModule } from 'nestjs-typegoose';
import { Face } from '../face-database/face.model';
import { PersonDatabaseService } from './person-database.service';
import { Person } from './person.model';

@Module({
    imports: [TypegooseModule.forFeature(Person), TypegooseModule.forFeature(Face)],
    providers: [PersonDatabaseService],
    exports: [PersonDatabaseService]
})
export class PersonDatabaseModule {}
