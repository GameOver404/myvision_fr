import { IsString } from 'class-validator';
import { arrayProp, prop, Typegoose } from 'typegoose';
import { Face } from '../face-database/face.model';

export class Person extends Typegoose {
    public _id!: string;

    @IsString()
    @prop({ required: true })
    public collectionId!: string;

    @IsString()
    @prop({ required: true })
    public name!: string;

    @arrayProp({ items: Face })
    public faces!: Face[];
}
