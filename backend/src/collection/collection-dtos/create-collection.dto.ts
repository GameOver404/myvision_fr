import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateCollectionDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly name!: string;
}
