import { CollectionDatabaseService } from '@database/collection-database/collection-database.service';
import { Test, TestingModule } from '@nestjs/testing';
import { DlibRecognitionService } from '@recognition/dlib-recognition/dlib-recognition.service';

import { CollectionService } from '@collection/collection.service';

fdescribe('CollectionService', () => {
    let service: CollectionService;

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                CollectionService,
                {
                    provide: 'CollectionDatabaseService',
                    useValue: 'collectionDatabaseSpy'
                },
                {
                    provide: 'DlibRecognitionService',
                    useValue: 'DlibRecognitionServiceSpy'
                }
            ]
        }).compile();
        service = module.get<CollectionService>(CollectionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
