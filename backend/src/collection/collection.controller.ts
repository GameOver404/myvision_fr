import { Body, Controller, Delete, Get, Param, Post, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { COLLECTION_ROUTE } from '../constants';
import { Collection } from '../database/collection-database/collection.model';
import { CreateCollectionDto } from './collection-dtos/create-collection.dto';
import { CollectionService } from './collection.service';

@ApiUseTags(COLLECTION_ROUTE)
@Controller(COLLECTION_ROUTE)
export class CollectionController {
    constructor(private readonly collectionService: CollectionService) {}

    @ApiOperation({ title: 'List Collections' })
    @Get()
    public list(): Promise<Collection[]> {
        return this.collectionService.list();
    }

    @ApiOperation({ title: 'Create collection' })
    @Post()
    public create(
        @Body(new ValidationPipe())
        createCollectionDto: CreateCollectionDto
    ): Promise<string> {
        return this.collectionService.create(createCollectionDto.name);
    }

    @ApiOperation({ title: 'Delete collection' })
    @Delete(':id')
    public delete(@Param('id') id: string): Promise<string> {
        return this.collectionService.delete(id);
    }
}
