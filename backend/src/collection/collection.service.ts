import { Injectable } from '@nestjs/common';
import { CollectionDatabaseService } from '../database/collection-database/collection-database.service';
import { Collection } from '../database/collection-database/collection.model';

@Injectable()
export class CollectionService {
    constructor(private readonly collectionDatabaseService: CollectionDatabaseService) {}

    public async list(): Promise<Collection[]> {
        return await this.collectionDatabaseService.listCollections();
    }

    public async create(name: string): Promise<string> {
        return await this.collectionDatabaseService.createCollection(name);
    }

    public async delete(id: string): Promise<string> {
        return await this.collectionDatabaseService.deleteCollection(id);
    }
}
