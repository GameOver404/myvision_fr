import { Module } from '@nestjs/common';
import { CollectionDatabaseModule } from '../database/collection-database/collection-database.module';
import { CollectionController } from './collection.controller';
import { CollectionService } from './collection.service';

@Module({
    imports: [CollectionDatabaseModule],
    controllers: [CollectionController],
    providers: [CollectionService]
})
export class CollectionModule {}
