import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { json } from 'body-parser';
import { AppModule } from './app.module';
import { APP_DESCRIPTION, APP_PORT, APP_TITLE, APP_VERSION, REQUEST_BODY_SIZE_LIMIT, SWAGGER_ROUTE } from './constants';

declare const module: any;

async function bootstrap() {
    // NestFactory creation
    const app = await NestFactory.create(AppModule);
    await app.use(
        json({
            limit: REQUEST_BODY_SIZE_LIMIT
        })
    );

    // Setup swagger
    const options = new DocumentBuilder()
        .setTitle(APP_TITLE)
        .setDescription(APP_DESCRIPTION)
        .setVersion(APP_VERSION)
        .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup(SWAGGER_ROUTE, app, document);

    // Start nest app
    await app.listen(APP_PORT);

    // Setup webpack hot reload
    if (module.hot) {
        module.hot.accept();
        module.hot.dispose(() => app.close());
    }
}

bootstrap();
