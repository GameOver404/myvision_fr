import { ConflictException } from '@nestjs/common';
import { FaceDescriptor } from 'face-recognition';

export interface DatabaseCollectionService {
    createCollection(name: string): Promise<string | ConflictException>;

    deleteCollection(id: string): Promise<string>;

    trainCollection(collectionId: string): Promise<FaceDescriptor[]>;
}
