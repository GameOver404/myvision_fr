import { Person } from '../database/person-database/person.model';

export interface DatabasePersonService {
    addPersonToCollection(collectionId: string, name: string): Promise<string>;

    deletePersonFromCollection(id: string): Promise<string>;

    listPersonsFromCollection(collectionId: string): Promise<Person[]>;
}
