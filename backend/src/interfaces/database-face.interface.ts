export interface DatabaseFaceService {
    addFaceToPerson(personId: string, name: string, faceDescriptors: number[]): Promise<string>;

    deleteFaceFromPerson(personId: string, id: string): Promise<string>;

    listFacesFromPerson(personId: string): Promise<FaceObject[]>;
}

export interface FaceObject {
    id: string;
    name: string;
}
