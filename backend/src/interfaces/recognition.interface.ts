import { Rect } from 'face-recognition';

export interface IdentifyFacesResult {
    id: string;
    name: string;
    confidence: number;
    rectangle: Rect;
}

export interface AgeDatabaseData {
    age: number;
    image: string;
}

export interface AgeTrainingsSet {
    age: number;
    image: Float32Array;
}

export interface AgeImageCounter {
    age: number;
    counter: number;
}

export interface EmotionDatabaseData {
    emotion: string;
    image: string;
    landmarks: number[];
}

export interface EmotionTrainingData {
    emotion: string;
    image: Float32Array;
    landmarks: number[];
}
