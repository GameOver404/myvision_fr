import { Module } from '@nestjs/common';
import { CollectionDatabaseModule } from '../database/collection-database/collection-database.module';
import { FaceDatabaseModule } from '../database/face-database/face-database.module';
import { PersonDatabaseModule } from '../database/person-database/person-database.module';
import { EmotionDatabaseModule } from '../database/train-database/emotion-database/emotion-database.module';
import { AgeDatabaseModule } from '../database/train-database/age-database/age-database.module';

import { AgeDetectionModule } from './age-detection/age-detection.module';
import { LivenessDetectionModule } from './liveness-detection/liveness-detection.module';
import { EmotionDetectionModule } from './emotion-detection/emotion-detection.module';
import { DlibRecognitionModule } from './dlib-recognition/dlib-recognition.module';
import { OpenCvRecognitionModule } from './opencv-recognition/opencv-recognition.module';
import { RecognitionController } from './recognition.controller';
import { RecognitionService } from './recognition.service';

@Module({
    exports: [RecognitionService],
    imports: [
        DlibRecognitionModule,
        OpenCvRecognitionModule,
        AgeDetectionModule,
        LivenessDetectionModule,
        FaceDatabaseModule,
        CollectionDatabaseModule,
        AgeDatabaseModule,
        PersonDatabaseModule,
        EmotionDetectionModule,
        EmotionDatabaseModule,
    ],
    controllers: [RecognitionController],
    providers: [RecognitionController, RecognitionService]
})
export class RecognitionModule {}
