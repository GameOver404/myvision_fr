import { Injectable } from '@nestjs/common';
import { loadModel, Model, range } from '@tensorflow/tfjs';
import { dot, sum, Tensor, tensor } from '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-node';
import { unlinkSync } from 'fs';
import { AGE_DETECTION_MODEL_PATH, TRAINING_ERROR_METHOD, TRAINING_OPTIMIZER } from '../../constants';
import { AgeTrainingsSet } from '../../interfaces/recognition.interface';

@Injectable()
export class AgeDetectionService {
    public async detectAgeSmall(imgData: Float32Array): Promise<number> {
        return this.detectAge(imgData);
    }


    public async trainAgeModel(trainingsSetSmall: AgeTrainingsSet[]): Promise<string> {
        try {
            const onlyValidTrainDataSmall = trainingsSetSmall.filter(data => data.image);
            await this.trainModel(onlyValidTrainDataSmall);
            return 'Models successfully trained';
        } catch (error) {
            throw new Error(error);
        }
    }

    private async loadModel(modelPath: string): Promise<Model> {
        return await loadModel(modelPath);
    }

    private async detectAge(imgData: Float32Array): Promise<number> {
        const ageTensor = this.getTensor(imgData);
        const ageModel = await this.loadModel(this.getModelFilePath());
        const prediction = this.getPrediction(ageModel, ageTensor);
        const numTens = this.getNumTens();
        return dot(prediction.dataSync(), numTens.dataSync()).get();
    }

    private getNumTens() {
        return range(6, 96).reshape([-1, 1]);
    }
    
    private getTensor(imgData: Float32Array) {
        return tensor(imgData, [1, 128, 128, 3]).div(tensor([256]));
    }
    private getTrainingLabel(data: AgeTrainingsSet): Tensor {
        return tensor(this.getAutofill(90, data.age - 6), [1, 90]);
    }

    private getTrainingValue(data: AgeTrainingsSet): Tensor {
        return tensor(data.image, [1, 128, 128, 3]);
    }
    private async trainModel(trainingsSet: AgeTrainingsSet[]) {
        const filePath = this.getModelFilePath();
        const model = await this.loadModel(filePath);
        await model.compile({
            optimizer: TRAINING_OPTIMIZER,
            loss: TRAINING_ERROR_METHOD
        });
        for (const data of trainingsSet) {
            const label = this.getTrainingLabel(data);
            const value = this.getTrainingValue(data);
            await model.fit(value, label, { epochs: 5 });
            await model.evaluate(value, label);
        }
        unlinkSync(this.getModelPath());
        await model.save(filePath.replace('model.json', ''));

    }

    private getPrediction(ageModel: any, ageTensor: Tensor): Tensor {
        return ageModel.predict(ageTensor);
    }

    private getAutofill(length: number, age?: number): number[] {
        let array = new Array(length).fill(0);
        age ? (array[age] = 1) : (array = [129.1863, 104.7624, 93.594].concat(array));
        return array;
    }

    private getModelFilePath(): string {
        return `file://${this.getModelPath()}`;
    }

    private getModelPath(): string {
        return __dirname + AGE_DETECTION_MODEL_PATH;
    }
}
