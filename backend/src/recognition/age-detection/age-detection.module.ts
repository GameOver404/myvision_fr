import { Module } from '@nestjs/common';
import { AgeDetectionService } from './age-detection.service';

@Module({
    providers: [AgeDetectionService],
    exports: [AgeDetectionService]
})
export class AgeDetectionModule {}
