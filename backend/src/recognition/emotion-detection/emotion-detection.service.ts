import { Injectable } from '@nestjs/common';
import { loadModel } from '@tensorflow/tfjs';
import { Tensor, tensor, tidy } from '@tensorflow/tfjs-core';
import '@tensorflow/tfjs-node';
import { unlinkSync } from 'fs';
import { EMOTIONS } from '../../constants';
import { EmotionTrainingData } from '../../interfaces/recognition.interface';

@Injectable()
export class EmotionDetectionService {
    public async detectEmotion(img: Float32Array, landmarks: number[]): Promise<string> {
        let face = tensor(img, [1, 48, 48, 3]);
        face = face.div(tensor([256]));
        const faceLandmarks = tensor(landmarks, [1, 68, 2]);
        const model = await loadModel('file://' + __dirname + '/models/model_emotion/model.json');
        const prediction = (await model.predict([face, faceLandmarks])) as Tensor;
        const predictionArray = Array.prototype.slice.call(prediction.dataSync());
        let predictedEmotion = 'unknown';
        let highest = 0;
        for (let emotion = 0; emotion < predictionArray.length; emotion++) {
            if (predictionArray[emotion] > highest) {
                highest = predictionArray[emotion];
                predictedEmotion = EMOTIONS[emotion];
            }
        }
        faceLandmarks.dispose();
        face.dispose();
        return predictedEmotion;
    }

    public async train(trainingData: EmotionTrainingData[]): Promise<string> {
        const model = await loadModel('file://' + __dirname + '/models/model_emotion/model.json');
        await model.compile({
            optimizer: 'sgd',
            loss: 'categoricalCrossentropy'
        });
        trainingData = trainingData.filter(data => data.image.length > 0);
        for (const data of trainingData) {
            const value = tensor(data.image, [1, 48, 48, 3]);
            const lands = tensor(data.landmarks, [1, 68, 2]);
            const expectedSolution = tensor(this.getCorrectSolution(data.emotion), [1, 7]);
            await model.fit([value, lands], expectedSolution, { epochs: 5 });
            await model.evaluate([value, lands], expectedSolution);
            value.dispose();
            lands.dispose();
        }

        await unlinkSync(__dirname + '/models/model_emotion/model.json');
        await model.save('file://' + __dirname + '/models/model_emotion');
        return 'Emotion Model successfully trained';
    }

    private getCorrectSolution(emotion: string): number[] {
        return EMOTIONS.map(emotionCategory => (emotionCategory === emotion ? 1 : 0));
    }
}
