import { Module } from '@nestjs/common';
import { EmotionDetectionService } from './emotion-detection.service';

@Module({
    providers: [EmotionDetectionService],
    exports: [EmotionDetectionService]
})
export class EmotionDetectionModule {}
