import { Injectable } from '@nestjs/common';

import { CascadeClassifier, HAAR_FRONTALFACE_ALT2, HAAR_FRONTALFACE_ALT, imdecode, Mat } from 'opencv4nodejs';

@Injectable()
export class OpenCvRecognitionService {
    public getCvImage(base64: string): Mat {
        const pngPrefix = 'data:image/png;base64,';
        const jpgPrefix = 'data:image/jpeg;base64,';
        const base64Data = base64.replace(pngPrefix, '').replace(jpgPrefix, '');
        const buffer = Buffer.from(base64Data, 'base64');
        return imdecode(buffer);
    }

    public getTensorImg(base64: string): Float32Array {
        return this.getImage(base64, 128, 128);
    }


    public getEmotionImage(base64: string): Float32Array {
        const img = this.getCvImage(base64);
        const classifier = new CascadeClassifier(HAAR_FRONTALFACE_ALT);
        const rect = classifier.detectMultiScale(img.bgrToGray()).objects[0];
        const image = rect
            ? img
                  .getRegion(rect)
                  .resize(48, 48)
                  .getData()
            : [];
        return new Float32Array(image);
    }

    private getImage(base64: string, rows: number, cols: number): Float32Array {
        const img = this.getCvImage(base64);
        const classifier = new CascadeClassifier(HAAR_FRONTALFACE_ALT2);
        const rect = classifier.detectMultiScale(img.bgrToGray()).objects[0];
        if (rect) {
            // rect = rect.pad(1.4); Cause for random std:Runtime Exceptions
            const image = img.getRegion(rect).resize(rows, cols);

            return new Float32Array(image.getData());
        } else {
            throw new Error('No face detected');
        }
    }
}
