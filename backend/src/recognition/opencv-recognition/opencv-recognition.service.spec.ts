import { Test, TestingModule } from '@nestjs/testing';

import { OpenCvRecognitionService } from '@recognition/opencv-recognition/opencv-recognition.service';
import { testData } from '@recognition/test-pictures/test-data.spec';

fdescribe('CollectionService', () => {
    let service: OpenCvRecognitionService;
    const testImages = testData;
    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            providers: [OpenCvRecognitionService]
        }).compile();
        service = module.get<OpenCvRecognitionService>(OpenCvRecognitionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('should return an opencv image if the parameter is a decoded base64 png ', () => {
        expect(service.getCvImage(testImages[0].picture).sizes).toEqual([150, 150]);
    });

    it('should return an opencv image if the parameter is a decoded base64 jpeg ', () => {
        expect(service.getCvImage(testImages[1].picture).sizes).toEqual([400, 630]);
    });
});
