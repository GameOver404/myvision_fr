import { Module } from '@nestjs/common';
import { OpenCvRecognitionService } from './opencv-recognition.service';

@Module({
    providers: [OpenCvRecognitionService],
    exports: [OpenCvRecognitionService]
})
export class OpenCvRecognitionModule {}
