import { Body, Controller, Get, Post, ValidationPipe } from '@nestjs/common';
import { ApiOperation, ApiUseTags } from '@nestjs/swagger';
import { MmodRect } from 'face-recognition';
import { RECOGNITION_ROUTE } from '../constants';
import { AgeImageCounter, IdentifyFacesResult } from '../interfaces/recognition.interface';
import { AddFaceImageDto } from './recognition-dtos/add-face-image.dto';
import { AddEmotionImageDto } from './recognition-dtos/add-emotion-image.dto';
import { DetectEmotionsDto } from './recognition-dtos/detect-emotions.dto';
import { DetectFacesDto } from './recognition-dtos/detect-faces.dto';
import { DetectLivenessDto } from './recognition-dtos/detect-liveness.dto';
import { IdentifyFacesDto } from './recognition-dtos/identify-faces.dto';
import { DetectAgeDto } from './recognition-dtos/detect-age.dto';
import { RecognitionService } from './recognition.service';

@ApiUseTags(RECOGNITION_ROUTE)
@Controller(RECOGNITION_ROUTE)
export class RecognitionController {
    constructor(private readonly recognitionService: RecognitionService) {}

    @ApiOperation({ title: 'detect Faces' })
    @Post('/detect')
    public detectFaces(
        @Body(new ValidationPipe())
        detectFacesDto: DetectFacesDto
    ): Promise<MmodRect[]> {
        return this.recognitionService.detectFaces(detectFacesDto.image);
    }

    @ApiOperation({ title: 'identifyFaces' })
    @Post('/identify')
    public identifyFaces(
        @Body(new ValidationPipe())
        identifyFacesDto: IdentifyFacesDto
    ): Promise<IdentifyFacesResult[]> {
        return this.recognitionService.identifyFaces(identifyFacesDto.collectionId, identifyFacesDto.image);
    }

    @ApiOperation({ title: 'detectEmotions' })
    @Post('/emotions')
    public detectEmotions(
        @Body(new ValidationPipe())
        detectEmotionsDto: DetectEmotionsDto
    ) {
        return this.recognitionService.detectEmotion(detectEmotionsDto.image);
    }

    @ApiOperation({ title: 'addEmotionImage' })
    @Post('emotions/addImage')
    public addEmotionImage(
        @Body(new ValidationPipe())
        addEmotionImageDto: AddEmotionImageDto
    ) {
        return this.recognitionService.addEmotionImage(addEmotionImageDto.emotion, addEmotionImageDto.image);
    }

    @ApiOperation({ title: 'trainEmotionModel' })
    @Post('emotions/train')
    public trainEmotionModel() {
        return this.recognitionService.trainEmotionModel();
    }

    @ApiOperation({ title: 'detectAge' })
    @Post('/age')
    public detectAge(
        @Body(new ValidationPipe())
        detectFacesDto: DetectAgeDto
    ) {
        return this.recognitionService.detectAge(detectFacesDto.image);
    }

    @ApiOperation({ title: 'detectLiveness' })
    @Post('/liveness')
    public detectLiveness(
        @Body(new ValidationPipe())
        detectFacesDto: DetectLivenessDto
    ) {
        return this.recognitionService.detectLiveness(detectFacesDto.image);
    }

    @ApiOperation({ title: 'addAgeImage' })
    @Post('/addAgeImage')
    public addAgeImage(
        @Body(new ValidationPipe())
        addFaceImageDto: AddFaceImageDto
    ) {
        return this.recognitionService.addAgeTrainingImage(addFaceImageDto.age, addFaceImageDto.image);
    }

    @ApiOperation({ title: 'trainAgeModel' })
    @Post('/trainAgeModel')
    public trainAgeModel(): Promise<String> {
        return this.recognitionService.trainAgeModel();
    }

    @ApiOperation({ title: 'getAgeImageCount' })
    @Get('/ageImageCount')
    public ageImageCount(): Promise<AgeImageCounter[]> {
        return this.recognitionService.getAgeImageCount();
    }
}
