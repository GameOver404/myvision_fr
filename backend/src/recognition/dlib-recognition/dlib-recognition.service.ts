import { Injectable } from '@nestjs/common';
import {
    CvImage,
    cvImageToImageRGB,
    FaceDescriptor,
    FaceDetector,
    FaceLandmark68Predictor,
    FaceRecognizer,
    FrontalFaceDetector,
    FullObjectDetection,
    ImageRGB,
    MmodRect,
    withCv
} from 'face-recognition';
import * as cv from 'opencv4nodejs';
import { IdentifyFacesResult } from '../../interfaces/recognition.interface';

@Injectable()
export class DlibRecognitionService {
    constructor() {
        withCv(cv);
    }

    public addFaceToPerson(image: cv.Mat, personId: string): number[] {
        const recognizer = FaceRecognizer();
        const detector: FaceDetector = FaceDetector();
        const faceImages = detector.detectFaces(cvImageToImageRGB(new CvImage(image)));
        recognizer.addFaces(faceImages, personId, 12);
        const modelState = recognizer.serialize();
        return modelState[0].faceDescriptors[0];
    }

    public detectFaces(image: cv.Mat): MmodRect[] {
        const detector: FaceDetector = FaceDetector();
        const rgbImage = cvImageToImageRGB(new CvImage(image));
        return detector.locateFaces(rgbImage);
    }

    public identifyFaces(image: cv.Mat, trainData: FaceDescriptor[]): IdentifyFacesResult[] {
        const detector = FaceDetector();
        const rgbImage = cvImageToImageRGB(new CvImage(image));
        const recognizer = FaceRecognizer();
        recognizer.load(trainData);
        return detector.detectFaces(rgbImage).map((face: ImageRGB, index: number) => {
            const candidate = recognizer.predictBest(face, 0.6);
            return {
                id: candidate.className,
                name: candidate.className,
                confidence: (1 - candidate.distance) * 100,
                rectangle: detector.locateFaces(rgbImage)[index].rect
            };
        });
    }

    public getFaceLandmarks(image: cv.Mat): FullObjectDetection[] {
        const detector = new FrontalFaceDetector();
        const face68LandmarkPredictor = FaceLandmark68Predictor();
        const rgbImage = cvImageToImageRGB(new CvImage(image));
        const faceRects = detector.detect(rgbImage);
        return faceRects.map((rect: any) => face68LandmarkPredictor.predict(rgbImage, rect));
    }

    
    public getFaceLandmarksEmo(image: cv.Mat): number[] {
        const detector = new FrontalFaceDetector();
        const face68LandmarkPredictor = FaceLandmark68Predictor();
        const rgbImage = cvImageToImageRGB(new CvImage(image));
        const faceRects = detector.detect(rgbImage);
        const lands = faceRects.map((rect: any) => face68LandmarkPredictor.predict(rgbImage, rect));
        const libLands = lands[0].getParts();
        let tensorArray: number[] = [];
        libLands.map(point => {
            tensorArray.push(point.y);
            tensorArray.push(point.x);
        });
        let sum = 0;
        tensorArray.map(val => (sum = sum + val));
        const mean = sum / 136;
        tensorArray = tensorArray.map(val => val - mean);
        return tensorArray;
    }
}
