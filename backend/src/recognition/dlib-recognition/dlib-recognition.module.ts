import { Module } from '@nestjs/common';
import { DlibRecognitionService } from './dlib-recognition.service';

@Module({
    providers: [DlibRecognitionService],
    exports: [DlibRecognitionService]
})
export class DlibRecognitionModule {}
