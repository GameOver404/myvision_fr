import { Test, TestingModule } from '@nestjs/testing';
import { imdecode, Mat } from 'opencv4nodejs';

import { mockCollection, testData } from '../test-pictures/test-data.spec';
import { DlibRecognitionService } from './dlib-recognition.service';

describe('CollectionService', () => {
    let service: DlibRecognitionService;
    const testImages = testData;
    const getImg = (base64: string): Mat => {
        const pngPrefix = 'data:image/png;base64,';
        const jpgPrefix = 'data:image/jpeg;base64,';
        const base64Data = base64.replace(pngPrefix, '').replace(jpgPrefix, '');
        const buffer = Buffer.from(base64Data, 'base64');
        return imdecode(buffer);
    };

    beforeAll(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [],
            providers: [DlibRecognitionService]
        }).compile();
        service = module.get<DlibRecognitionService>(DlibRecognitionService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    describe('detectFaces', () => {
        it('should return the correct rectangle', () => {
            const img = getImg(testImages[2].picture);
            expect(service.detectFaces(img)[0].confidence).toBe(1.0114015340805054);
        });

        it('should return an empty Array if no face is in the picture', () => {
            const img = getImg(testImages[3].picture);
            expect(service.detectFaces(img).length).toBe(0);
        });

        it('should return four rectangle  ', () => {
            const img = getImg(testImages[4].picture);
            expect(service.detectFaces(img).length).toBe(4);
        });
    });

    describe('getFaceLandmarks', () => {
        it('should return the correct landmarks', () => {
            const img = getImg(testImages[2].picture);
            expect(service.getFaceLandmarks(img)[0].getParts()[0].x).toEqual(-4);
        });
    });

    describe('identifyFaces', () => {
        it('should return the correct face', () => {
            const img = getImg(testImages[1].picture);
            expect(service.identifyFaces(img, mockCollection)[0].name).toEqual('Daryl');
        });
        it('should return unknown', () => {
            const img = getImg(testImages[2].picture);
            expect(service.identifyFaces(img, mockCollection)[0].name).toEqual('unknown');
        });
    });
});
