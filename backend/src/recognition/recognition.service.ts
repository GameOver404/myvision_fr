import { Injectable } from '@nestjs/common';
import { MmodRect } from 'face-recognition';
import { CollectionDatabaseService } from '../database/collection-database/collection-database.service';
import { FaceDatabaseService } from '../database/face-database/face-database.service';
import { PersonDatabaseService } from '../database/person-database/person-database.service';
import { EmotionDatabaseService } from '../database/train-database/emotion-database/emotion-database.service';
import { AgeDatabaseService } from '../database/train-database/age-database/age-database.service';
import { AgeDatabaseData, AgeImageCounter, IdentifyFacesResult } from '../interfaces/recognition.interface';
import { AgeDetectionService } from './age-detection/age-detection.service';
import { LivenessDetectionService } from './liveness-detection/liveness-detection.service';
import { DlibRecognitionService } from './dlib-recognition/dlib-recognition.service';
import { EmotionDetectionService } from './emotion-detection/emotion-detection.service';
import { OpenCvRecognitionService } from './opencv-recognition/opencv-recognition.service';

@Injectable()
export class RecognitionService {
    constructor(
        private readonly faceDatabaseService: FaceDatabaseService,
        private readonly dlibRecognitionService: DlibRecognitionService,
        private readonly collectionDatabaseService: CollectionDatabaseService,
        private readonly openCvRecognitionService: OpenCvRecognitionService,
        public readonly ageDetectionService: AgeDetectionService,
        public readonly livenessDetectionService: LivenessDetectionService,
        public readonly ageDataBaseService: AgeDatabaseService,
        private readonly personDatabaseService: PersonDatabaseService,
        private readonly emotionDatabaseService: EmotionDatabaseService,
        private readonly emotionDetectionService: EmotionDetectionService
    ) {}

    public async addFaceToPerson(personId: string, image: string): Promise<number[]> {
        const cvImg = this.openCvRecognitionService.getCvImage(image);
        return await this.dlibRecognitionService.addFaceToPerson(cvImg, personId);
    }
    
    public async detectFaces(image: string): Promise<MmodRect[]> {
        const cvImg = this.openCvRecognitionService.getCvImage(image);
        return this.dlibRecognitionService.detectFaces(cvImg);
    }

    public async identifyFaces(collectionId: string, image: string): Promise<IdentifyFacesResult[]> {
        const trainedData = await this.collectionDatabaseService.trainCollection(collectionId);
        const cvImg = this.openCvRecognitionService.getCvImage(image);
        let result = await this.dlibRecognitionService.identifyFaces(cvImg, trainedData);

        result = await Promise.all(
            result.map(async person => ({
                id: person.id,
                name: person.id !== 'unknown' ? (await this.personDatabaseService.getPerson(person.id)).name : 'unknown',
                confidence: Math.round(person.confidence),
                rectangle: person.rectangle
            }))
        );

        return result;
    }

    public async detectAge(image: string): Promise<number> {
        try {
            const imageArray = this.openCvRecognitionService.getTensorImg(image);
            const age = await this.ageDetectionService.detectAgeSmall(imageArray);
            return Math.round(age);
        } catch (err) {
            throw err;
        }
    }

    public async detectLiveness(image: string): Promise<number> {
        try {
            const imageArray = this.openCvRecognitionService.getTensorImg(image);
            const liveness = await this.livenessDetectionService.binaryClassification(imageArray);
            return Math.round(liveness);
        } catch (err) {
            throw err;
        }
    }

    public async trainAgeModel(): Promise<string> {
        const ageCluster = await this.ageDataBaseService.loadAgeData();
        const trainData = ageCluster.map(data => ({
            age: data.age,
            image: this.openCvRecognitionService.getTensorImg(data.image)
        }));
        return await this.ageDetectionService.trainAgeModel(trainData);
    }

    public async addAgeTrainingImage(age: number, image: string): Promise<string> {
        return await this.ageDataBaseService.addAgeImage(age, image);
    }

    public async getAgeImageCount(): Promise<AgeImageCounter[]> {
        return await this.ageDataBaseService
            .loadAgeData()
            .then(data => [
                ...data
                    .reduce(
                        (acc: Map<number, AgeImageCounter>, curr: AgeDatabaseData) =>
                            acc.get(curr.age)
                                ? acc.set(curr.age, { age: curr.age, counter: acc.get(curr.age)!.counter + 1 })
                                : acc.set(curr.age, { age: curr.age, counter: 1 }),
                        new Map()
                    )
                    .values()
            ]);
    }

    public async detectEmotion(image: string): Promise<string> {
        const imgArray = this.openCvRecognitionService.getEmotionImage(image);
        const landmarks = await this.detectLandmarks(image);
        return await this.emotionDetectionService.detectEmotion(imgArray, landmarks);
    }

    public async addEmotionImage(emotion: string, image: string): Promise<string> {
        const cvImg = this.openCvRecognitionService.getCvImage(image);
        const landmarks = this.dlibRecognitionService.getFaceLandmarksEmo(cvImg);
        return await this.emotionDatabaseService.addEmotionImage(emotion, image, landmarks);
    }

    public async trainEmotionModel(): Promise<string> {
        const databaseData = await this.emotionDatabaseService.getTrainingData();
        const trainData = await Promise.all(
            databaseData.map(data => ({
                emotion: data.emotion,
                image: this.openCvRecognitionService.getEmotionImage(data.image),
                landmarks: data.landmarks
            }))
        );
        return await this.emotionDetectionService.train(trainData);
    }

    private async detectLandmarks(image: string): Promise<number[]> {
        const cvImg = this.openCvRecognitionService.getCvImage(image);
        return this.dlibRecognitionService.getFaceLandmarksEmo(cvImg);
    }
}
