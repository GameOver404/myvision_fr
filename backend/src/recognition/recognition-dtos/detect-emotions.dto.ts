import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class DetectEmotionsDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly image!: string;
}
