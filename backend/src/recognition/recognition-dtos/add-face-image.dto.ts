import { ApiModelProperty } from '@nestjs/swagger';
import { IsInt, IsNotEmpty, IsPositive, IsString, Max } from 'class-validator';

export class AddFaceImageDto {
    @ApiModelProperty()
    @IsInt()
    @IsNotEmpty()
    @IsPositive()
    @Max(100)
    public readonly age!: number;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly image!: string;
}
