import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class IdentifyFacesDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly collectionId!: string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly image!: string;
}
