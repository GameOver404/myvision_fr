import { ApiModelProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class DetectAgeDto {
    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly image!: string;
}
