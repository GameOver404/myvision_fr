import { ApiModelProperty } from '@nestjs/swagger';
import { IsIn, IsNotEmpty, IsString } from 'class-validator';
import { EMOTIONS } from '../../constants';

export class AddEmotionImageDto {
    @ApiModelProperty()
    @IsNotEmpty()
    @IsIn(EMOTIONS)
    public readonly emotion!:string;

    @ApiModelProperty()
    @IsString()
    @IsNotEmpty()
    public readonly image!: string;
}


