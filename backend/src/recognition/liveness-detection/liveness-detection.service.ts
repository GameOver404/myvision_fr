import { Injectable } from '@nestjs/common';
import { loadModel, Model, range } from '@tensorflow/tfjs';
import { dot, Tensor, tensor } from '@tensorflow/tfjs-core';
import { LIVENESS_DETECTION_MODEL_PATH} from '../../constants';

@Injectable()
export class LivenessDetectionService {
    public async binaryClassification(imgData: Float32Array): Promise<number> {
        return this.detectLiveness(imgData);
    }

private async loadModel(modelPath: string): Promise<Model> {
    return await loadModel(modelPath);
}

//commented out due to incorrect transformation of the binary classification model
private async detectLiveness(imgData: Float32Array): Promise<number> {
    //const livenessTensor = this.getTensor(imgData);
    //const livenessModel = await this.loadModel(this.getModelFilePath());
    //const prediction = this.getPrediction(livenessModel, livenessTensor);
    //const numTens = this.getNumTens();
    //return dot(prediction.dataSync(), numTens.dataSync()).get();
    return 99;
}

private getNumTens() {
    return range(0, 100).reshape([-1, 1]);
}

private getTensor(imgData: Float32Array) {
    return tensor(imgData, [1, 24, 100, 100, 1]).div(tensor([64]));
}

private getPrediction(livenessModel: any, livenessTensor: Tensor): Tensor {
    return livenessModel.predict(livenessTensor);
}

private getModelFilePath(): string {
    return `file://${this.getModelPath()}`;
}

private getModelPath(): string {
    return __dirname + LIVENESS_DETECTION_MODEL_PATH;
}

}