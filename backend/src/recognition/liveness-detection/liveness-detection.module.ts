import { Module } from '@nestjs/common';
import { LivenessDetectionService } from './liveness-detection.service';

@Module({
    providers: [LivenessDetectionService],
    exports: [LivenessDetectionService]
})
export class LivenessDetectionModule {}
