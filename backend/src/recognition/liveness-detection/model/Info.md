# Informationen zur Implementierung (der Binären Klassifizierung)

## Transformation der Binären Klassifizierung

Die Binäre Klassifizierung wurde soweit in die Webanwendung übernommen, allerdings wird die Umwandlung des Keras-Modells - trotz unterstützter Methoden - nicht korrekt ausgeführt.

Sowohl die Ausführung des Batch-Befehls als auch eine Umwandlung mittels des von TensorFlow.js vorgegebenen Codes führten dazu, dass die modelTopology den Wert 'null' zugewiesen bekam.
Die modelTopology enthält Informationen über den Aufbau des neuronalenen Netzes, wie es beispielsweise bei der model.json der Alterserkennung ersichtlich ist. 
Die Gewichte wurden hingegen korrekt erzeugt. Des Weiteren wurde versucht die von Keras erstellte model.json zu verwenden, was ebenfalls fehlschlug, da TensorFlow.js erkannte, dass diese mithilfe von Python generiert wurde.
Auch ein Versuch die model.json 1:1 von Hand in die korrekte Form umzuschreiben war nicht erfolgreich, da die von TensorFlow.js gewünschte Registrierung nicht korrekt ausgeführt werden konnte.

## Stand der Webanwendung

Diese Applikation bietet aufgrund des oben-genannten Problems lediglich einen gewissen Schutz gegen 3D-Angriffe, welcher durch die Alterserkennung geboten wird.

Der auskommentierte Programmcode der Binären Klassifizierung sollte mit einer korrekt erstellten model.json fehlerfreie Ergebnisse liefern.

## Problematik durch TensorFlow.js 

Da TensorFlow.js eine recht junge und leider noch recht fehleranfällige Bibliothek ist, wurde der Code der Binären Klassifizierung - mit Ausnahme der Daten in Swagger - auskommentiert.
[Swagger](https://swagger.io/) dient der einachen Erstellung von Application Programming Interfaces (APIs).

Das TensorFlow.js noch sehr fehleranfällig ist, zeigt sich u. A. darin, dass häufig Binding-Fehler auftreten. Bindings werden benötigt, um sicherzustellen, dass mit Bibliotheken 
kommunizieren werden kann. Bindungen sind ausführbarer Code, durch den Bibliotheken angesprochen werden können und diese wiederum Antworten liefern können. 

Die Binding-Probleme konnten lediglich mit einem Downgrade auf eine ältere TensorFlow.js-Version behoben werden, weswegen die package.json angepasst wurde, sodass
"@tensorflow/tfjs-node": "0.1.17" installiert wird. Mit dieser Version traten, im Gegensatz zu den neueren Versionen, keine Binding-Fehler auf.

## Alternative Anwendung

Die Anwendung der Binären Klassifizierung wurde schon zuvor um eine Gesichtsidentifizierung (mit Lock-Algorithmus) ergänzt. 

Dieses Anwendung schützt gegen 2D-, Video und 3D-Angriffe und erfüllt überdies alle in der Bachelorarbeit gestellten Anforderungen, weswegen hier auf diese verwiesen wird:
[Applikation der Binären Klassifizierung](https://gitlab.com/GameOver404/liveness_classification)


