# Face Recognition Backend (myVision)

This project was generated with [Nest.js CLI](https://github.com/nestjs/nest-cli) version 5.1.2.

## Description

Node.js Backend for Computer Vision (CV) tasks build with Nest.js (Express, MongoDB, Mongoose, ...) and OpenCV, CV (dlib). [SourceCode in TypeScript]

## Prerequisites

This project requires the following dependencies to be installed beforehand:
* node.js
* Python and pip (especially for Windows)
* vs2017 for Windows
* npm
* CMake for Node.js OpenCV server
* xQuartz/X11, libpng for CV tasks (dlib)
* MongoDB for Node.js server as the database
* MongoDB Compass is recommended to have a good view over the database

How to install node.js and npm you can check in the root [readme - Install Node.js and NPM](../README.md#Prerequisites).

#### Install CMake
Cmake can easily be installed via Homebrew, too:
```
brew install cmake
```

To install cmake on Windows please make sure that Python2 or Python3 are installed on your machine.
If that is not the case, download either version from: [Python.org/downloads](https://www.python.org/downloads/).
Python comes with pip, so you can simply run:
```
pip install cmake
```

If there are any problems during this run, please check that your Pyhon folder and the Python/scripts folder is in your PATH variable. (Running pip3 install cmake or pip2 install cmake might help as well.)

#### Install xQuartz / X11
xQuartz can easily be installed via Homebrew:
```
brew cask install xquartz
```

On Windows XQUartz does not exist, but X11 does

To install X Window System Server please visit [https://sourceforge.net/projects/xming/](https://sourceforge.net/projects/xming/) and follow the information on screen

#### Install libpng
libpng can easily be installed via Homebrew:
``` 
brew install libpng
```

On Windows please visit [http://gnuwin32.sourceforge.net/packages/libpng.htm](http://gnuwin32.sourceforge.net/packages/libpng.htm) to install libpng

#### Install MongoDB
MongoDB can easily be installed via Homebrew:
```
brew install mongodb
```

Otherwise visit [docs.MongoDB for installation](https://docs.mongodb.com/manual/installation/)

Configure your mongoDB server
```
# create the db directory
mkdir -p /data/db (macOs / Unix)
md \data\db (Windows)

# Moreover, log files are recommended
mkdir -p /data/log (macOS / Unix)
md /data/log (Windows)

# give the db correct read/write permissions (macOS/Linux)
chmod 777 /data/db
```
On DOS/Windows permissions are handled differently and therefore nothing has to be done when one uses Windows. 

Start your mongoDB server (You'll probably want another command prompt.)
```
mongod
```

In case mongod doesn't work, make sure that the path to the file is in the PATH variale.

## Dependencies

To install all needed dependencies just run:
```shell
npm install
```

## Running the app

The app address is: [http://localhost:3000/](http://localhost:3000/), but all API-Calls start with **api/v1/**.

#### development
```bash
$ npm run start
```

### Swagger (API-Doku)

The API documentation can be found under: [http://localhost:3000/swagger/](http://localhost:3000/swagger/).

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test-e2e

# test coverage
$ npm run test-cov
```
