/*****************************************************************
 * This is the development (DEV) environment configuration.      *
 *****************************************************************/

import { environmentBase } from './environment.base';

export const environment = { ...environmentBase };

// Only change properties that need to be changed from the base environment!
environment.envName = 'DEV';
