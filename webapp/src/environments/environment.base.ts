/*****************************************************************
 * This is the base (BASE) environment configuration.            *
 * This BASE environment is used as the default value.            *
 *****************************************************************/

export const EnvironmentBase = {
    appVersion: '0.1.0-LOCAL',
    envName: 'BASE',
    production: false,
    logLevel: 'debug',
    companyId: '000' 
};

export const environmentBase = EnvironmentBase;
