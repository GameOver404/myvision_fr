import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Candidate } from '../face-recognition.interface';

@Component({
    selector: 'app-person-tile',
    templateUrl: './person-tile.component.html',
    styleUrls: ['./person-tile.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonTileComponent {
    @Input() person: Candidate;
}
