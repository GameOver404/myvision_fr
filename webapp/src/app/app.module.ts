import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WebCamModule } from 'ack-angular-webcam';
import { HttpClientModule } from '@angular/common/http';
import { OurFaceRecognitionService } from './our-face-recognition.service';
import { PersonTileComponent } from './person-tile/person-tile.component';
import { FaceRecognitionFactory } from './face-recognition.factory';
import { FaceRecognitionManagerComponent } from './face-recognition-manager/face-recognition-manager.component';
import { FaceRecognitionComponent } from './face-recognition/face-recognition.component';
import { OurNodeFaceRecognitionService } from './our-node-face-recognition.service';
import { WebcamComponent } from './webcam/webcam.component';
import { UserNotificationComponent } from './user-notification/user-notification.component';
import { UserNotificationService } from './user-notification.service';
import { FaceRecognitionShowcaseComponent } from './face-recognition-showcase/face-recognition-showcase.component';
import { SvgIconComponent } from './svg-icon/svg-icon.component';
import { AddUserShowcaseComponent } from './add-user-showcase/add-user-showcase.component';
import {
    MatAutocompleteModule,
    MatCardModule,
    MatDialogModule,
    MatDividerModule,
    MatInputModule,
    MatProgressSpinnerModule
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GreetingCardComponent } from './greeting-card/greeting-card.component';
import { ButtonComponent } from './button/button.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ThumbnailComponent } from './thumbnail/thumbnail.component';
import { NavigationSidebarComponent } from './navigation-sidebar/navigation-sidebar.component';

@NgModule({
    declarations: [
        AppComponent,
        PersonTileComponent,
        FaceRecognitionManagerComponent,
        FaceRecognitionComponent,
        WebcamComponent,
        UserNotificationComponent,
        FaceRecognitionShowcaseComponent,
        SvgIconComponent,
        AddUserShowcaseComponent,
        GreetingCardComponent,
        ButtonComponent,
        ThumbnailComponent,
        NavigationSidebarComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        RouterModule,
        WebCamModule,
        HttpClientModule,
        BrowserAnimationsModule,
        MatCardModule,
        MatDividerModule,
        ReactiveFormsModule,
        MatDialogModule,
        MatInputModule,
        MatAutocompleteModule,
        MatProgressSpinnerModule,
    ],
    providers: [
        OurFaceRecognitionService,
        OurNodeFaceRecognitionService,
        FaceRecognitionFactory,
        UserNotificationService,
    ],
    bootstrap: [AppComponent],
    entryComponents: [AddUserShowcaseComponent]
})
export class AppModule {}
