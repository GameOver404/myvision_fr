import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Candidate, FaceRecognitionResult } from '../face-recognition.interface';

@Component({
    selector: 'app-greeting-card',
    templateUrl: './greeting-card.component.html',
    styleUrls: ['./greeting-card.component.scss']
})
export class GreetingCardComponent {
    public candidate: Candidate;
    public isKnown: boolean;

    @Input('recognitionResult')
    set allowDay(value: FaceRecognitionResult[]) {
        if (value.length > 0) {
            this.candidate = value[0].candidates[0];
            this.isKnown = value[0].isKnown;
        } else {
            this.isKnown = false;
            this.candidate = null;
        }
    }

    @Output() buttonClicked: EventEmitter<any> = new EventEmitter<any>();

    public addFaceToPerson(isKnown: boolean) {
        this.buttonClicked.emit(isKnown ? this.candidate : null);
    }
}
