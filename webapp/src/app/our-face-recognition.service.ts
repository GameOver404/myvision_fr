import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {
    CreatePersonResult,
    FaceDetectionResult,
    FaceRecognitionResult,
    FaceRecognitionService,
    Person,
    PersonCollection
} from './face-recognition.interface';
import { catchError, map } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';

export interface OurFaceRecognitionResponse {
    status: string;
    result: OurFaceRecognitionResult[];
}

export interface OurFaceRecognitionResult {
    age: number;
    age_uncertainty: number;
    //liveness: number;
    rect_points: OurFaceRect;
    predictions: OurPrediction[];
    is_known: boolean;
}

export interface OurPrediction {
    person_id: string;
    name: string;
    probability: number;
    avatar: string;
}

export interface OurFaceRect {
    top_left: OurCoordinate;
    width: number;
    height: number;
}

export interface OurCoordinate {
    x: number;
    y: number;
}

export interface OurSearchPersonResponse {
    status: string;
    result: OurPerson[];
}

export interface OurPerson {
    _id: string;
    name: string;
    avatar: string;
}

@Injectable()
export class OurFaceRecognitionService implements FaceRecognitionService {
    readonly API_BASE_URL = 'https://localhost:5000/';
    readonly API_DETECT_URL = this.API_BASE_URL;
    readonly API_IDENTIFY_URL = this.API_BASE_URL + 'face/person/analyze';
    readonly API_GET_FACE_URL = this.API_BASE_URL + 'data/imgs/';

    constructor(private http: HttpClient) {}

    private prepareImages(images: string[]) {
        return images.join('__');
    }

    public detectFaces(image: string[]): Observable<FaceDetectionResult[]> {
        return this.getGenericError();
    }

    public identifyFacesInCollection(images: string[], collectionId: string): Observable<FaceRecognitionResult[]> {
        const formData = new FormData();
        formData.append('image', images[0]);
        formData.append('age', 'true');
        //formData.append('liveness', 'true');
        formData.append('identify', 'true');

        return this.http.post<OurFaceRecognitionResponse>(this.API_IDENTIFY_URL, formData).pipe(
            map(response => response.result),
            map(
                results =>
                    results.map(
                        result =>
                            <FaceRecognitionResult>{
                                rectangle: {
                                    width: result.rect_points.width,
                                    height: result.rect_points.height,
                                    left: result.rect_points.top_left.x,
                                    top: result.rect_points.top_left.y
                                },
                                attributes: {
                                    age: Math.round(result.age),
                                    age_uncertainty: Math.round(result.age_uncertainty * 10) / 10
                                    //liveness : Math.round(result.liveness)
                                },
                                candidates: result.predictions.map(
                                    candidate =>
                                        <Person>{
                                            id: candidate.person_id,
                                            name: candidate.name,
                                            confidence: Math.round(candidate.probability * 100),
                                            avatar: candidate.avatar
                                        }
                                ),
                                isKnown: result.is_known
                            }
                    ),
                catchError((error: HttpErrorResponse) => new ErrorObservable(error.error))
            )
        );
    }

    public getFacesForId(personId: string): Observable<string[]> {
        return of([this.API_GET_FACE_URL + personId + '.png']);
    }

    public addPersonCollection(id: string): Observable<any> {
        return this.getGenericError();
    }

    public deletePersonCollection(id: string): Observable<any> {
        return this.getGenericError();
    }

    public addPersonToCollection(name: string, collectionId: string, image?: string): Observable<CreatePersonResult> {
        const formData = new FormData();
        formData.append('frames', image);
        formData.append('name', name);
        formData.append('company_id', collectionId);
        formData.append('position', '');
        return this.http.post<any>(this.API_BASE_URL + 'face/person/add_person', formData).pipe(catchError(this.handleError));
    }

    public deletePersonFromCollection(personId: string, collectionId: string): Observable<any> {
        return this.getGenericError();
    }

    public getAllPersonsFromCollection(id: string): Observable<any> {
        return this.getGenericError();
    }

    public addFacesToPersonInCollection(images: string[], personId: string, collectionId: string): Observable<any> {
        const formData = new FormData();
        formData.append('frames', this.prepareImages(images));
        return this.http
            .post<any>(this.API_BASE_URL + 'face/person/add_images_to_person/' + personId, formData)
            .pipe(catchError(this.handleError));
    }

    public trainCollection(id: string): Observable<any> {
        return this.http.get<any>(this.API_BASE_URL + 'face/person/train').pipe(catchError(this.handleError));
    }

    public getPersonFromCollection(id: string): Observable<Person> {
        return this.getGenericError();
    }

    private getGenericError() {
        return new ErrorObservable('This Feature is not available with this service at the moment.');
    }

    public getPersonCollections(): Observable<PersonCollection[]> {
        return of([{ id: 'test', name: 'test' }]);
    }

    public searchPerson(name: string): Observable<Person[]> {
        return this.http.get<OurSearchPersonResponse>(this.API_BASE_URL + 'face/person/search/' + name).pipe(
            map(response =>
                response.result.map(
                    person =>
                        <Person>{
                            id: person._id,
                            name: person.name,
                            avatar: person.avatar
                        }
                )
            ),
            catchError(this.handleError)
        );
    }

    private handleError(error: HttpErrorResponse) {
        return new ErrorObservable(error.message);
    }
}
