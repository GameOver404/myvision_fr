import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { timer } from 'rxjs/observable/timer';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

@Injectable()
export class UserNotificationService {
    public visible$ = new BehaviorSubject<boolean>(false);
    public message$ = new BehaviorSubject<string>('');
    public error$ = new BehaviorSubject<boolean>(false);
    public success$ = new BehaviorSubject<boolean>(false);
    private timerSub$ = new Subscription();

    showSuccessMessage = (message: string) => {
        this.error$.next(false);
        this.success$.next(true);
        this.showMessage(message);
    };

    showErrorMessage = (message: string) => {
        this.error$.next(true);
        this.success$.next(false);
        this.showMessage(message);
    };

    showMessage = (message: string) => {
        this.message$.next(message);
        this.visible$.next(true);
        this.timerSub$.unsubscribe();
        this.timerSub$ = timer(3000, 0)
            .pipe(first())
            .subscribe(this.hide);
    };

    hide = () => {
        this.visible$.next(false);
        this.message$.next('');
        this.success$.next(false);
        this.error$.next(false);
    };
}
