import { TestBed, inject } from '@angular/core/testing';

import { OurNodeFaceRecognitionService } from './our-node-face-recognition.service';

describe('OurNodeFaceRecognitionService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [OurNodeFaceRecognitionService]
        });
    });

    it('should be created', inject([OurNodeFaceRecognitionService], (service: OurNodeFaceRecognitionService) => {
        expect(service).toBeTruthy();
    }));
});
