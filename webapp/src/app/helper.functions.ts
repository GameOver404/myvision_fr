import { pipe } from 'rxjs/util/pipe';
import { filter, map } from 'rxjs/operators';

export class HelperFunctions {
    public static isNull$ = filter(value => value === null);

    public static notNull$ = filter(value => !!value);

    public static notEmpty$ = filter((value: any[]) => !!value && value.length > 0);

    public static arrayNotEmpty$ = filter((value: Array<any>) => value.length > 0);

    public static arrayIsEmpty$ = map((value: Array<any>) => !!value.length);

    public static arrayNotNullNotEmpty$ = pipe(
        HelperFunctions.notNull$,
        HelperFunctions.arrayNotEmpty$
    );

    public static arrayNotNullIsEmpty$ = pipe(
        HelperFunctions.notNull$,
        HelperFunctions.arrayIsEmpty$
    );

    public static base64ToBlob(image: string) {
        const byteString = atob(image.split(',')[1]);
        const ab = new ArrayBuffer(byteString.length);
        const ia = new Uint8Array(ab);
        for (let i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i);
        }
        return new Blob([ab]);
    }
}
