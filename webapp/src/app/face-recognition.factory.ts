import { Injectable, Injector } from '@angular/core';
import { FaceRecognitionService, FaceRecServiceProvider } from './face-recognition.interface';
import { OurNodeFaceRecognitionService } from './our-node-face-recognition.service';

@Injectable()
export class FaceRecognitionFactory {
    private serviceCache = new Map<FaceRecServiceProvider, FaceRecognitionService>();

    constructor(private injector: Injector) {}

    private getService(provider: FaceRecServiceProvider, service: any) {
        if (!this.serviceCache.has(provider)) {
            this.serviceCache.set(provider, this.injector.get(service));
        }
        return this.serviceCache.get(provider);
    }

    getServiceForProvider(provider: FaceRecServiceProvider): FaceRecognitionService {
        return this.getService(FaceRecServiceProvider.MYVISION, OurNodeFaceRecognitionService);
        }
    }
