import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-navigation-sidebar',
    templateUrl: './navigation-sidebar.component.html',
    styleUrls: ['./navigation-sidebar.component.scss']
})
export class NavigationSidebarComponent implements OnInit {
    public navigationItems = [
        {
            title: 'Recognition',
            click: () => {
                this.router.navigate(['']);
            }
        },
        {
            title: 'myVision Manager',
            click: () => {
                this.router.navigate(['/manager/myVision']);
            }
        },
    ];

    constructor(public router: Router) {}
    ngOnInit() {}
}
