import { Component, OnInit, ViewChild } from '@angular/core';
import { WebcamComponent } from '../webcam/webcam.component';
import { Candidate, FaceRecognitionResult, FaceRecognitionService, FaceRecServiceProvider } from '../face-recognition.interface';
import { FaceRecognitionFactory } from '../face-recognition.factory';
import { combineLatest, filter, first, map } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UserNotificationService } from '../user-notification.service';
import { MatDialog } from '@angular/material';
import { AddUserShowcaseComponent } from '../add-user-showcase/add-user-showcase.component';
import { HelperFunctions } from '../helper.functions';
import { environment } from '../../environments/environment';

@Component({
    selector: 'app-face-recognition-showcase',
    templateUrl: './face-recognition-showcase.component.html',
    styleUrls: ['./face-recognition-showcase.component.scss']
})
export class FaceRecognitionShowcaseComponent implements OnInit {
    public webcamSize = {
        height: 0,
        width: 0
    };
    public recognitionRunning$ = new BehaviorSubject<boolean>(false);
    @ViewChild(WebcamComponent) private webcam: WebcamComponent;
    private readonly faceRecProvider = FaceRecServiceProvider.MYVISION;
    private selectedService: FaceRecognitionService;
    private readonly captureInterval = 500;
    public recognitionResult = new BehaviorSubject<FaceRecognitionResult[]>(null);

    constructor(
        private faceRecognitionFactory: FaceRecognitionFactory,
        private notificationService: UserNotificationService,
        private dialog: MatDialog
    ) {
        this.selectedService = this.faceRecognitionFactory.getServiceForProvider(this.faceRecProvider);
    }

    ngOnInit() {
        this.webcamSize.width = 1920;
        this.webcamSize.height = 1080;
    }

    public toggleRecognition() {
        if (this.recognitionRunning$.getValue()) {
            this.webcam.stopCapturingFramesInIntervall();
            this.webcam.faces = [];
        } else {
            this.webcam.startCapturingFramesInInterval(this.captureInterval);
        }
        this.recognitionRunning$.next(!this.recognitionRunning$.getValue());
    }

    public newWebcamFrame(image) {
        this.selectedService
            .identifyFacesInCollection([image], environment.companyId)
            .pipe(
                combineLatest(this.recognitionRunning$, (recResult, running) => ({ recResult: recResult, running: running })),
                filter(result => result.running),
                map(result => result.recResult)
            )
            .subscribe(
                result => {
                    this.recognitionResult.next(result);
                },
                error => this.notificationService.showErrorMessage(error)
            );
    }

    public webcamInit(success: boolean) {
        if (success) {
            this.toggleRecognition();
            this.recognitionResult.pipe(HelperFunctions.notNull$).subscribe((result: FaceRecognitionResult[]) => {
                if (result.length > 0) {
                    this.webcam.faces = [result[0]];
                } else {
                    this.webcam.faces = [];
                }
            });
        }
    }

    public showPersonDialog(person: Candidate = null) {
        this.webcam.playPauseWebcam();
        this.toggleRecognition();
        const dialogRef = this.dialog.open(AddUserShowcaseComponent, { data: person, width: '600px' });
        dialogRef
            .afterClosed()
            .pipe(first())
            .subscribe(() => {
                this.webcam.playPauseWebcam();
                this.toggleRecognition();
            });
    }
}
