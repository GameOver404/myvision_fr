import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { WebCamComponent } from 'ack-angular-webcam';
import { Options } from 'ack-angular-webcam/videoHelp';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { timer } from 'rxjs/observable/timer';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { FaceRecognitionResult } from '../face-recognition.interface';

const enum WEBCAM_ACTION {
    Play = 'Play',
    Pause = 'Pause',
    Start = 'Start',
    Stop = 'Stop'
}

export enum WEBCAM_MODE {
    embedded = 'embedded',
    popup = 'popup',
    presentation = 'presentation'
}

@Component({
    selector: 'app-webcam',
    templateUrl: './webcam.component.html',
    styleUrls: ['./webcam.component.scss']
})
export class WebcamComponent implements OnInit {
    @Input() width = 640;
    @Input() height = 480;
    @Input() faces: FaceRecognitionResult[];
    @Input() mode = WEBCAM_MODE.embedded;
    @Output() newestFrame: EventEmitter<string> = new EventEmitter<string>();
    @Output() camInit: EventEmitter<boolean> = new EventEmitter<boolean>();

    webcam: WebCamComponent; // will be populated by <ack-webcam [(ref)]="webcam">
    webcamDevices: MediaDeviceInfo[]; // will be populated by the <ack-media-devices [(array)]="webcamDevices"

    webcamPlayPauseButtonText = WEBCAM_ACTION.Play;
    webcamStartStopButtonText = WEBCAM_ACTION.Start;
    availableWebcamModes = WEBCAM_MODE;

    options: Options;

    private captureFrames$ = new Subject<boolean>();
    public visible$ = new BehaviorSubject<boolean>(true);

    ngOnInit() {
        this.options = {
            video: {
                height: this.height,
                width: this.width
            },
            audio: false,
            width: this.width,
            height: this.height,
            fallback: false,
            fallbackSrc: '',
            fallbackMode: '',
            fallbackQuality: 100
        };
        if (this.mode === WEBCAM_MODE.popup) {
            this.visible$.next(false);
        }
    }

    onCamError(err) {
        console.error('WebCam Init Error:', err);
        this.camInit.next(false);
    }

    onCamSuccess(suc) {
        console.log('WebCam Init Success:', suc);
        this.webcamPlayPauseButtonText = WEBCAM_ACTION.Pause;
        this.webcamStartStopButtonText = WEBCAM_ACTION.Stop;
        this.camInit.next(true);
    }

    isWebcamStarted = () => this.webcam && this.webcam.stream && this.webcam.stream.active;

    stopWebcam = () => this.webcam.stream.getTracks().forEach(track => track.stop());

    selectWebcam(deviceIndex: number) {
        this.stopWebcam();
        this.options = {
            ...this.options,
            video: {
                deviceId: this.webcamDevices[deviceIndex].deviceId
            }
        };
        this.webcam.setWebcam(this.options);
    }

    playPauseWebcam() {
        if (this.webcamPlayPauseButtonText === WEBCAM_ACTION.Play) {
            this.webcamPlayPauseButtonText = WEBCAM_ACTION.Pause;
            this.webcam.getVideoElm().play();
        } else {
            this.webcamPlayPauseButtonText = WEBCAM_ACTION.Play;
            this.webcam.getVideoElm().pause();
        }
    }

    startStopWebcam() {
        if (this.webcamStartStopButtonText === WEBCAM_ACTION.Start) {
            this.webcamStartStopButtonText = WEBCAM_ACTION.Stop;
            this.webcam.setWebcam(this.options);
        } else {
            this.webcamStartStopButtonText = WEBCAM_ACTION.Start;
            this.stopWebcam();
        }
    }

    captureImage() {
        this.webcam.getBase64('image/jpeg').then(image => {
            this.newestFrame.next(image);
        });
        if (this.mode === WEBCAM_MODE.popup) {
            this.hide();
        }
    }

    startCapturingFramesInInterval(interval: number) {
        this.captureFrames$.next(true);
        timer(0, interval)
            .pipe(takeUntil(this.captureFrames$))
            .subscribe(() => {
                this.captureImage();
            });
    }

    stopCapturingFramesInIntervall() {
        this.captureFrames$.next(false);
    }

    show = () => this.visible$.next(true);

    hide = () => this.visible$.next(false);
}
