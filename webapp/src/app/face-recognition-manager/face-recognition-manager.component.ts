import { Component, OnDestroy, ViewChild } from '@angular/core';
import { FaceRecognitionService, FaceRecServiceProvider, Person, PersonCollection } from '../face-recognition.interface';
import { first, map, takeUntil } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FaceRecognitionFactory } from '../face-recognition.factory';
import { ActivatedRoute } from '@angular/router';
import { Subject } from 'rxjs/Subject';
import { WEBCAM_MODE, WebcamComponent } from '../webcam/webcam.component';
import { UserNotificationService } from '../user-notification.service';
import { HelperFunctions } from '../helper.functions';

@Component({
    selector: 'app-face-recognition-manager',
    templateUrl: './face-recognition-manager.component.html',
    styleUrls: ['./face-recognition-manager.component.scss']
})
export class FaceRecognitionManagerComponent implements OnDestroy {
    private success$ = new Subject<string>();
    private error$ = new Subject<string>();
    public collections$ = new BehaviorSubject<PersonCollection[]>(null);
    public selectedCollection$ = new BehaviorSubject<PersonCollection>(null);
    public persons$ = new BehaviorSubject<Person[]>(null);
    public selectedPerson$ = new BehaviorSubject<Person>(null);
    private recognitionService: FaceRecognitionService;
    private destroy$ = new Subject();
    public image$ = new BehaviorSubject<string>(null);
    public webcamModes = WEBCAM_MODE;
    public provider = '';
    public personCollectionName = '';
    public personName = '';

    public collectionIsEmpty$ = this.collections$.pipe(HelperFunctions.arrayNotNullIsEmpty$);
    public personsIsEmpty$ = this.persons$.pipe(HelperFunctions.arrayNotNullIsEmpty$);

    @ViewChild(WebcamComponent) private webcam: WebcamComponent;

    constructor(
        private route: ActivatedRoute,
        private recognitionFactory: FaceRecognitionFactory,
        private notificationService: UserNotificationService
    ) {
        this.selectedCollection$
            .pipe(
                HelperFunctions.notNull$,
                takeUntil(this.destroy$)
            )
            .subscribe((collection: PersonCollection) => {
                this.getAllPersonsOfCollection(collection.id);
            });

        this.collections$
            .pipe(
                HelperFunctions.notNull$,
                takeUntil(this.destroy$)
            )
            .subscribe(collections => this.selectedCollection$.next(collections[0]));

        this.persons$
            .pipe(
                HelperFunctions.notNull$,
                takeUntil(this.destroy$)
            )
            .subscribe((persons: Person[]) => {
                this.selectedPerson$.next(persons[0]);
                if (persons.length === 0) {
                    this.selectedPerson$.next(null);
                }
            });

        this.error$.subscribe(this.notificationService.showErrorMessage);
        this.success$.subscribe(this.notificationService.showSuccessMessage);

        this.route.params.subscribe(params => {
            const provider = <FaceRecServiceProvider>FaceRecServiceProvider[params['provider'].toUpperCase()];
            if (provider) {
                this.provider = provider;
                this.recognitionService = recognitionFactory.getServiceForProvider(provider);

                this.getCollections();
            } else {
                // TODO route to 404 page
            }
        });
    }

    public addCollection() {
        this.recognitionService
            .addPersonCollection(this.personCollectionName)
            .pipe(
                first(),
                map(() => 'Person Group successfully created')
            )
            .subscribe(next => this.showSuccessUpdateCollections(next), error => this.showError(error));
    }

    public deleteCollection() {
        this.recognitionService
            .deletePersonCollection(this.selectedCollection$.getValue().id)
            .pipe(
                first(),
                map(() => 'Person Group successfully deleted')
            )
            .subscribe(next => this.showSuccessUpdateCollections(next), error => this.showError(error));
    }

    public addPersonToCollection(image?: string) {
        this.recognitionService
            .addPersonToCollection(this.personName, this.selectedCollection$.getValue().id, image)
            .pipe(
                first(),
                map(person => 'Person was successfully created with id' + person.personId)
            )
            .subscribe(next => this.showSuccessMessageUpdatePersons(next), error => this.showError(error));
    }

    public deletePersonFromCollection() {
        this.recognitionService
            .deletePersonFromCollection(this.selectedPerson$.getValue().id, this.selectedCollection$.getValue().id)
            .pipe(
                first(),
                map(() => 'Person was successfully deleted')
            )
            .subscribe(next => this.showSuccessMessageUpdatePersons(next), error => this.showError(error));
    }

    public addPhotoToPersonInCollection() {
        this.image$.pipe(first()).subscribe(image => {
            this.recognitionService
                .addFacesToPersonInCollection([image], this.selectedPerson$.getValue().id, this.selectedCollection$.getValue().id)
                .pipe(
                    first(),
                    map(() => 'Image was successfully added to Person')
                )
                .subscribe(next => this.showSuccessAndClearPhoto(next), error => this.showError(error));
        });
    }

    public trainCollection() {
        this.recognitionService
            .trainCollection(this.selectedCollection$.getValue().id)
            .pipe(
                first(),
                map(() => 'Collection was successfully trained')
            )
            .subscribe(next => this.showSuccess(next), error => this.showError(error));
    }

    public setPersonCollectionName(name: string) {
        this.personCollectionName = name;
    }

    public setPersonName(name: string) {
        this.personName = name;
    }

    public validateInput(input: string) {
        return input !== '';
    }

    public selectPersonCollection(index: number) {
        this.collections$.pipe(first()).subscribe(collection => this.selectedCollection$.next(collection[index]));
    }

    public selectPerson(index: number) {
        this.persons$.pipe(first()).subscribe(collection => this.selectedPerson$.next(collection[index]));
    }

    public newWebcamFrame(frame) {
        this.image$.next(frame);
    }

    public takePhoto() {
        this.webcam.show();
    }

    public fileInputChanged(event) {
        const reader = new FileReader();
        reader.onload = () => {
            this.image$.next(reader.result);
            event.srcElement.value = null;
        };
        reader.readAsDataURL(event.target.files[0]);
    }

    public clearPhoto() {
        this.image$.next(null);
    }

    private getCollections() {
        this.recognitionService
            .getPersonCollections()
            .pipe(first())
            .subscribe(collections => this.collections$.next(collections), error => this.showError(error));
    }

    private getAllPersonsOfCollection(id: string) {
        this.recognitionService
            .getAllPersonsFromCollection(id)
            .pipe(first())
            .subscribe(persons => this.persons$.next(persons), error => this.showError(error));
    }

    private showSuccessAndClearPhoto = (message: string) => {
        this.showSuccessMessageUpdatePersons(message);
        this.clearPhoto();
    };

    private showSuccessUpdateCollections = (message: string) => {
        this.showSuccess(message);
        this.getCollections();
    };

    private showSuccessMessageUpdatePersons(message: string) {
        this.showSuccess(message);
        this.getAllPersonsOfCollection(this.selectedCollection$.getValue().id);
    }

    private showSuccess = (success: string) => {
        this.success$.next(success);
    };

    private showError = (error: string) => {
        this.error$.next(error);
    };

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }
}
