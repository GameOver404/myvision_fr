import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-svg-icon',
    templateUrl: './svg-icon.component.html',
    styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent {
    @Input() id = '';
    @Input() size = 24;
    public path = './assets/icons/';
}
