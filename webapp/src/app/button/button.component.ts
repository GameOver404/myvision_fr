import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
    @Input() label: string;
    @Input() isDisabled = false;
    @Input() icon = '';
    @Input() iconSize = 16;
}
