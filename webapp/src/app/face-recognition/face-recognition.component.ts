import { Component, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FaceRecognitionFactory } from '../face-recognition.factory';
import { combineLatest, filter, first, map, mergeMap, takeUntil } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FaceAttributeParams, FaceRecognitionService, FaceRecServiceProvider, PersonCollection } from '../face-recognition.interface';
import { WebcamComponent } from '../webcam/webcam.component';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { UserNotificationService } from '../user-notification.service';
import { timer } from 'rxjs/observable/timer';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'app-face-recognition',
    templateUrl: './face-recognition.component.html',
    styleUrls: ['./face-recognition.component.scss']
})
export class FaceRecognitionComponent implements OnDestroy {
    @ViewChild(WebcamComponent) private webcam: WebcamComponent;

    private selectedService: FaceRecognitionService;
    private selectedFaceRecProvider$ = new BehaviorSubject<FaceRecServiceProvider>(FaceRecServiceProvider.MYVISION);
    private destroy$ = new Subject<boolean>();
    public attributes = ['age', 'emotion', 'all'];

    public recognitionRunning$ = new BehaviorSubject<boolean>(false);
    public captureInterval = 10000;
    public faceRecProviderKeys = Object.keys(FaceRecServiceProvider);

    public personCollections$: Observable<PersonCollection[]> = this.selectedFaceRecProvider$.pipe(
        mergeMap(service => this.faceRecognitionFactory.getServiceForProvider(service).getPersonCollections())
    );
    public selectedPersonCollection$ = new BehaviorSubject<PersonCollection>(null);
    public image$ = new BehaviorSubject<string>(null);
    public time$ = new Subject<number>();
    private timerSub: Subscription;

    constructor(
        private faceRecognitionFactory: FaceRecognitionFactory,
        private router: Router,
        private notificationService: UserNotificationService
    ) {
        this.personCollections$
            .pipe(
                filter(collection => collection.length > 0),
                takeUntil(this.destroy$)
            )
            .subscribe(
                collection => this.selectedPersonCollection$.next(collection[0]),
                error => this.notificationService.showErrorMessage(error)
            );

        this.selectedFaceRecProvider$
            .pipe(takeUntil(this.destroy$))
            .subscribe(provider => (this.selectedService = this.faceRecognitionFactory.getServiceForProvider(provider)));

        this.image$
            .pipe(
                filter(image => !!image),
                takeUntil(this.destroy$)
            )
            .subscribe(image => {
                this.selectedService
                    .identifyFacesInCollection([image], this.selectedPersonCollection$.getValue().id, {
                        identifyAll: true,
                        candidateCount: 3,
                        faceAttributes: [FaceAttributeParams.age]
                    })
                    .pipe(
                        combineLatest(this.recognitionRunning$, (recResult, running) => ({ recResult: recResult, running: running })),
                        filter(result => result.running),
                        map(result => result.recResult)
                    )
                    .subscribe(result => (this.webcam.faces = result), error => notificationService.showErrorMessage(error));
            });
    }

    setCapturingInterval(value: number) {
        this.captureInterval = value;
    }

    selectFaceRecService(index: number) {
        this.selectedFaceRecProvider$.next(FaceRecServiceProvider[this.faceRecProviderKeys[index]]);
        if (this.recognitionRunning$.getValue()) {
            this.stop();
        }
    }

    public newWebcamFrame(frame) {
        this.image$.next(frame);
    }

    selectPersonCollection(index: number) {
        this.personCollections$.pipe(first()).subscribe(collection => this.selectedPersonCollection$.next(collection[index]));
    }

    start() {
        this.recognitionRunning$.next(true);
        this.webcam.startCapturingFramesInInterval(this.captureInterval);
        this.startTimer();
    }

    stop() {
        this.recognitionRunning$.next(false);
        this.webcam.stopCapturingFramesInIntervall();
        this.webcam.faces = [];
        this.stopTimer();
    }

    startTimer() {
        this.timerSub = timer(0, 1000)
            .pipe(
                map(tick => this.captureInterval / 1000 - (tick % (this.captureInterval / 1000))),
                takeUntil(this.destroy$)
            )
            .subscribe(time => this.time$.next(time));
    }

    stopTimer() {
        this.timerSub.unsubscribe();
        this.time$.next(null);
    }

    ngOnDestroy() {
        this.destroy$.next(true);
        this.destroy$.unsubscribe();
    }
}
