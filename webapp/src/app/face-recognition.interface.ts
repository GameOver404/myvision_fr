import { Observable } from 'rxjs/Observable';

export interface FaceRecognitionService {
    readonly API_BASE_URL: string;
    readonly API_DETECT_URL: string;
    readonly API_IDENTIFY_URL: string;
    readonly API_GET_FACE_URL: string;

    detectFaces(images: string[]): Observable<FaceDetectionResult[]>;

    identifyFacesInCollection(images: string[], collectionId?: string, params?: FaceRecParams): Observable<FaceRecognitionResult[]>;

    getFacesForId(id: string): Observable<string[]>;

    addPersonCollection(id: string): Observable<any>;

    deletePersonCollection(id: string): Observable<any>;

    getPersonCollections(): Observable<PersonCollection[]>;

    addPersonToCollection(name: string, collectionId: string, image?: string): Observable<CreatePersonResult>;

    deletePersonFromCollection(personId: string, collectionId: string): Observable<any>;

    getAllPersonsFromCollection(id: string): Observable<Person[]>;

    getPersonFromCollection(id: string, collectionId: string): Observable<Person>;

    trainCollection(id: string): Observable<any>;

    addFacesToPersonInCollection(images: string[], personId: string, collectionId: string): Observable<AddFaceResponse>;

    searchPerson(name: string): Observable<Person[]>;
}

export interface AddFaceResponse {
    persistedFaceId: string;
}

export interface FaceDetectionResult {
    faceId: string;
    faceRectangle: FaceRectangle;
    faceAttributes: FaceAttributes;
}

export interface FaceAttributes {
    age: number;
    age_uncertainty?: number;
   // liveness: number;
}

export interface CreatePersonResult {
    personId: string;
}

export interface FaceRectangle {
    width: number;
    height: number;
    left: number;
    top: number;
    percent?: boolean;
}

export interface FaceRecognitionResult {
    rectangle?: FaceRectangle;
    candidates: Candidate[];
    attributes?: FaceAttributes;
    isKnown?: boolean;
}

export interface Person {
    id: string;
    name: string;
    avatar?: string;
    faceIds?: string[];
}

export interface Candidate extends Person {
    confidence: number;
}

export enum FaceRecServiceProvider {
    MYVISION = 'MyVision',
}

export interface PersonCollection {
    id: string;
    name: string;
}

export enum FaceAttributeParams {
    age = 'age',
    //liveness = 'alive',
}

export interface FaceRecParams {
    faceAttributes?: FaceAttributeParams[];
    identifyAll?: boolean;
    candidateCount?: number;
}

export interface OurPersonCollection {
    _id: string;
    name: string;
    faces?: OurFaces[];
}

export interface OurFaces {
    _id?: number;
    name: string;
    faceDescriptors: number[];
}

export interface OurRectangle {
    confidence: number;
    rect: {
        left: number;
        top: number;
        right: number;
        bottom: number;
        area: number;
    };
}

export interface OurCandidate {
    id: string;
    name: string;
    confidence: number;
    rectangle: {
        top: number;
        left: number;
        right: number;
        bottom: number;
    };
}
