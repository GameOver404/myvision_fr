import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { map, mergeMap, tap } from 'rxjs/operators';
import {
    CreatePersonResult,
    FaceDetectionResult,
    FaceRecognitionResult,
    FaceRecognitionService,
    FaceRecParams,
    Person,
    PersonCollection,
    OurCandidate,
    OurFaces,
    OurPersonCollection,
    OurRectangle
} from './face-recognition.interface';

@Injectable()
export class OurNodeFaceRecognitionService implements FaceRecognitionService {
    readonly API_KEY = '';
    readonly API_BASE_URL = 'http://localhost:3000/api/v1/';
    readonly API_RECOGNITION_URL = this.API_BASE_URL + 'recognition/';
    readonly API_COLLECTION_URL = this.API_BASE_URL + 'collection/';
    readonly API_PERSON_URL = this.API_BASE_URL + 'person/';
    readonly API_FACE_URL = this.API_BASE_URL + 'face';
    // set correct API path
    readonly API_DETECT_URL = this.API_RECOGNITION_URL + 'detect';
    readonly API_IDENTIFY_URL = this.API_RECOGNITION_URL + 'identify';
    readonly API_AGE_URL = this.API_RECOGNITION_URL + 'age';
    readonly API_GET_FACE_URL: string;

    constructor(private http: HttpClient) {}

    detectFaces(images: string[]): Observable<FaceDetectionResult[]> {
        const body = {
            image: images[0]
        };
        return this.http.post(this.API_DETECT_URL, body).pipe(
            map((rectangles: OurRectangle[]) =>
                rectangles.map(
                    rectangle =>
                        <FaceDetectionResult>{
                            faceId: '',
                            faceRectangle: {
                                width: rectangle.rect.right - rectangle.rect.left,
                                left: rectangle.rect.left,
                                top: rectangle.rect.top,
                                height: rectangle.rect.bottom - rectangle.rect.top
                            },
                            faceAttributes: {}
                        }
                )
            )
        );
    }

    identifyFacesInCollection(images: string[], collectionId?: string, params?: FaceRecParams): Observable<FaceRecognitionResult[]> {
        const body = {
            image: images[0],
            collectionId: collectionId
        };
        return this.http.post(this.API_IDENTIFY_URL, body).pipe(
            map((candidates: OurCandidate[]) =>
                candidates.map(
                    candidate =>
                        <FaceRecognitionResult>{
                            candidates: [
                                {
                                    name: candidate.name,
                                    id: candidate.id,
                                    confidence: candidate.confidence
                                }
                            ],
                            rectangle: {
                                top: candidate.rectangle.top,
                                left: candidate.rectangle.left,
                                height: candidate.rectangle.bottom - candidate.rectangle.top,
                                width: candidate.rectangle.right - candidate.rectangle.left
                            }
                        }
                )
            ),
            mergeMap(
                () => this.getAge(images[0]),
                (candidates, ageResult) => candidates.map(candidate => ({ ...candidate, attributes: { age: ageResult } }))
            )
        );
    }

    getFacesForId(id: string): Observable<string[]> {
        return this.http.get(this.API_FACE_URL + id).pipe(map((faces: OurFaces[]) => faces.map(face => face.toString())));
    }

    addPersonCollection(id: string): Observable<string> {
        const body = {
            name: id
        };
        return this.http.post(this.API_COLLECTION_URL, body).pipe(map((message: string) => message));
    }

    deletePersonCollection(id: string): Observable<string> {
        return this.http.delete(this.API_COLLECTION_URL + id).pipe(map((message: string) => message));
    }

    getPersonCollections(): Observable<PersonCollection[]> {
        return this.http.get(this.API_COLLECTION_URL).pipe(
            map((collections: OurPersonCollection[]) =>
                collections.map(
                    collection =>
                        <PersonCollection>{
                            id: collection._id,
                            name: collection.name
                        }
                )
            )
        );
    }

    addPersonToCollection(name: string, collectionId: string, image?: string): Observable<CreatePersonResult> {
        const body = {
            collectionId: collectionId,
            name: name
        };
        return this.http.post(this.API_PERSON_URL, body).pipe(map((message: string) => <CreatePersonResult>{ personId: message }));
    }

    deletePersonFromCollection(personId: string, collectionId: string): Observable<string> {
        return this.http.delete(this.API_PERSON_URL + personId).pipe(map((message: string) => message));
    }

    getAllPersonsFromCollection(id: string): Observable<Person[]> {
        return this.http.get(this.API_PERSON_URL + id).pipe(
            map((persons: OurPersonCollection[]) =>
                persons.map(
                    person =>
                        <PersonCollection>{
                            id: person._id,
                            name: person.name,
                            faceIds: person.faces.map(face => face._id)
                        }
                )
            )
        );
    }

    getPersonFromCollection(id: string, collectionId: string): Observable<Person> {
        return of();
    }

    trainCollection(id: string): Observable<any> {
        return of([]);
    }

    addFacesToPersonInCollection(images: string[], personId: string, collectionId: string): Observable<any> {
        const body = {
            personId: personId,
            name: 'test',
            image: images[0]
        };
        return this.http.post(this.API_FACE_URL, body).pipe();
    }
    getAge(image): Observable<number> {
        const body = {
            image: image
        };
        return <Observable<number>>this.http.post(this.API_AGE_URL, body).pipe();
    }
    searchPerson(name: string): Observable<Person[]> {
        return of([]);
    }
}
