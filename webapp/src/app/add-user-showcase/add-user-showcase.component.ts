import { AfterViewInit, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { WebcamComponent } from '../webcam/webcam.component';
import { FormControl } from '@angular/forms';
import { Candidate, FaceRecognitionService, FaceRecServiceProvider, Person } from '../face-recognition.interface';
import { FaceRecognitionFactory } from '../face-recognition.factory';
import { fromEvent } from 'rxjs/observable/fromEvent';
import { Observable } from 'rxjs/Observable';
import { mergeMap, scan, switchMap, take } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';
import { timer } from 'rxjs/observable/timer';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { HelperFunctions } from '../helper.functions';
import { environment } from '../../environments/environment';

@Component({
    selector: 'app-add-user-showcase',
    templateUrl: './add-user-showcase.component.html',
    styleUrls: ['./add-user-showcase.component.scss']
})
export class AddUserShowcaseComponent implements OnInit, AfterViewInit {
    public webcamSize = {
        width: 550,
        height: 550 * (9 / 16)
    };

    @ViewChild(WebcamComponent) webcamComp: WebcamComponent;
    @ViewChild('nameInput') nameInput: ElementRef;
    public formCtrl: FormControl;
    public options$: Observable<Person[]>;
    public countdown$: Observable<number>;
    public previewImages$: Observable<string[]>;
    public isLoading = false;
    public newPersonImages: string[] = [];
    public photoTriggerActive = false;
    private firstCamSelected = false;
    private readonly faceRecProvider = FaceRecServiceProvider.MYVISION;
    private selectedService: FaceRecognitionService;
    private webcamFrames$ = new BehaviorSubject<string>(null);

    constructor(
        public dialogRef: MatDialogRef<AddUserShowcaseComponent>,
        private faceRecognitionFactory: FaceRecognitionFactory,
        @Inject(MAT_DIALOG_DATA) public data: Candidate
    ) {
        this.selectedService = this.faceRecognitionFactory.getServiceForProvider(this.faceRecProvider);
    }

    ngOnInit() {
        console.log('DATA', this.data);
        this.formCtrl = new FormControl(!!this.data ? this.data : '');
    }

    ngAfterViewInit(): void {
        this.options$ = fromEvent(this.nameInput.nativeElement, 'keyup').pipe(
            switchMap(() => {
                if (this.formCtrl.value.length > 0) {
                    return this.selectedService.searchPerson(this.formCtrl.value);
                } else {
                    return of([]);
                }
            })
        );
    }

    cancelDialog() {
        this.dialogRef.close();
    }

    camInit(init: boolean) {
        if (!this.firstCamSelected && init) {
            const options = {
                ...this.webcamComp.options,
                video: {
                    deviceId: this.webcamComp.webcamDevices[0].deviceId
                }
            };
            this.webcamComp.webcam.setWebcam(options);
            this.firstCamSelected = true;
        }
    }

    displayPersonWith(person: Person) {
        if (!!person) {
            return person.name;
        }
    }

    addPerson() {
        console.log('Add Person', this.formCtrl.value);
        if (!!this.formCtrl.value && this.newPersonImages.length > 0) {
            // add to person
            this.isLoading = true;
            if (this.formCtrl.value.id) {
                this.selectedService
                    .addFacesToPersonInCollection(this.newPersonImages, this.formCtrl.value.id, environment.companyId)
                    .pipe(mergeMap(() => this.selectedService.trainCollection('')))
                    .subscribe(
                        null,
                        error => {
                            console.log('On Error ', error);
                            this.isLoading = false;
                        },
                        () => {
                            console.log('On Complete ');
                            this.isLoading = false;
                            this.cancelDialog();
                        }
                    );
            } else {
                // new person
                this.selectedService
                    .addPersonToCollection(this.formCtrl.value, environment.companyId, this.newPersonImages.join('__'))
                    .pipe(mergeMap(() => this.selectedService.trainCollection('')))
                    .subscribe(
                        () => {
                            console.log('Train Collection Success');
                        },
                        () => {
                            this.isLoading = false;
                        },
                        () => {
                            this.isLoading = false;
                            this.cancelDialog();
                        }
                    );
            }
        }
    }

    triggerPhotos() {
        if (!this.photoTriggerActive) {
            this.photoTriggerActive = true;
            console.log('Make Photos');
            this.countdown$ = timer(0, 1000).pipe(
                scan(acc => acc - 1, 4),
                take(4)
            );

            this.countdown$.subscribe(null, null, this.makePhotos);
        }
    }

    makePhotos = () => {
        this.webcamComp.startCapturingFramesInInterval(500);
        this.previewImages$ = this.webcamFrames$.pipe(
            HelperFunctions.notNull$,
            scan((acc, value: string) => acc.concat(value), []),
            take(5)
        );
        this.previewImages$.subscribe(
            (image: string[]) => {
                console.log('image', image);
                this.newPersonImages = image;
            },
            null,
            () => {
                this.webcamComp.stopCapturingFramesInIntervall();
                this.photoTriggerActive = false;
            }
        );
    };

    newestFrame(frame) {
        console.log('Frame');
        this.webcamFrames$.next(frame);
    }
}
