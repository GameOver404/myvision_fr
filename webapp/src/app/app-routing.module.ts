import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FaceRecognitionManagerComponent } from './face-recognition-manager/face-recognition-manager.component';
import { FaceRecognitionComponent } from './face-recognition/face-recognition.component';
import { FaceRecognitionShowcaseComponent } from './face-recognition-showcase/face-recognition-showcase.component';

const routes: Routes = [
    {
        path: '',
        component: FaceRecognitionComponent,
        pathMatch: 'full'
    },
    {
        path: 'manager/:provider',
        component: FaceRecognitionManagerComponent,
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
