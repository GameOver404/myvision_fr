import { Component } from '@angular/core';
import { UserNotificationService } from '../user-notification.service';

@Component({
    selector: 'app-user-notification',
    templateUrl: './user-notification.component.html',
    styleUrls: ['./user-notification.component.scss']
})
export class UserNotificationComponent {
    constructor(public notificationService: UserNotificationService) {}
}
