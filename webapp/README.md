# Computer Vision myVision frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Description

Angular Frontend for Computer Vision (CV) tasks build with AngularCLI. [SourceCode in TypeScript]

## Prerequisites
This project requires the following dependencies to be installed beforehand:
* node.js
* npm

## Dependencies
All needed dependencies can be installed with npm install or npm i and afterwards they can be found in the respective node_modules folder.

If there's the need to update a dependency it is advised to run npm update -module, instead of npm update, since
it can't be guaranteed that the web app works with newer versions of every package. 
Certainly an Angular update would be problematic, since we do NOT have an angular.json file. (Moreover, the angular-cli.json file can not be used as a substitute!)

## Development server

Run `npm start` for a dev server. Navigate to [http://localhost:4200/](http://localhost:4200/). The app will automatically reload, if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
